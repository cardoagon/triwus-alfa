<?php

//~ error_reporting(0);
error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);

//------------------------------------------------


$site_home  = "../../";
$trw_home   = "{$site_home}triwus/";
$tilia_home = "{$trw_home}tilia/";

//------------------------------------------------

require "{$tilia_home}manifesto.php";
require "{$trw_home}manifesto.php";

require "{$trw_home}php/rpc/trwrest/TRW_rest.php";
require "{$trw_home}php/rpc/trwrest/TrwRestCliente.php";
require "{$trw_home}php/rpc/facturascripts/FSCliente.php";



//------------------------------------------------


$c = new FSCliente([id_site], $_REQUEST);

exit($c->exec());
