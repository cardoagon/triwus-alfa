<?php


abstract class FS_ehtml extends CLista_ehtml {
  const style_tr   = "";
  const style_tr_a = "style='background-color: #fcf5d5;'";


  public function __construct(Iterador_bd $rbd = null) {
    parent::__construct($rbd);

    $this->class_table = "lista_00";
  }

  protected function __bmais_html($id_rama, $readonly = false, $css = null) {
    if ($this->xestor->sublista_prototipo($id_rama) == null) {
      return "";
    }
    else {
      $readonly_aux = $readonly;

      if (!$this->rama_check($id_rama)) {
        $id = "brama_mais";
        $etq = "▶";
        $tit = "Expandir rama";
      }
      else {
        $id = "brama_menos";
        $etq = "▼";
        $tit = "Contraer rama";
      }
    }

    $b = self::__boton($id, $tit, $etq, $readonly_aux, $css);

    $b->clase_css("default", "lista_bmais");
    $b->clase_css("readonly", "lista_bmais");

    return $this->__fslControl($b, $id_rama)->html();
  }

  protected function trw_paxinador(string $action):string { //* deshuso
    if ($this->xestor->limit_f == null) return "";

    $i = $this->xestor->__i();

    if (($t = $this->xestor->__t()) < 2) return "";


    $a = self::__boton("lpax_a", "", "&#9668;", $i == 1 );
    $s = self::__boton("lpax_s", "", "&#9658;", $i == $t);

    if ($i > 1) {
      $p = $i - 1;
      
      //~ $a->pon_eventos("onclick", "document.location = '{$action}&pax={$p}'");
      $a->pon_eventos("onclick", "trwCat_paxinar(this, '{$action}', {$p})");
    }

    if ($i < $t) {
      $p = $i + 1;
      
      //~ $s->pon_eventos("onclick", "document.location = '{$action}&pax={$p}'");
      $s->pon_eventos("onclick", "trwCat_paxinar(this, '{$action}', {$p})");
    }
    
    $i2 = self::__text($this, "ti", $i      );
    $t2 = self::__text($this, "tt", $t, true);

    $i2->pon_eventos("onmouseover", "this.style.borderColor = '#ccc'"             );
    $i2->pon_eventos("onfocus"    , "this.style.borderColor = '#ccc'"             );
    $i2->pon_eventos("onblur"     , "this.style.borderColor = 'transparent'"      );
    $i2->pon_eventos("onchange"   , "trwCat_paxinar_txt(this, '{$action}', {$t})" );

    return $this->__fslControl($a)->html() . "&nbsp;" . $i2->html() . "&nbsp;de&nbsp;" . $t2->html() . "&nbsp;" . $this->__fslControl($s)->html();
  }
}
