<?php


abstract class XPax_loms2 extends FSL_navelmt {
  public $tipo;
  public $id_idioma;
  public $id_pax_select;

  public function __construct($tipo, $id_opcion, $id_idioma) {
    parent::__construct("tPaxinas", new XPax_loms2_ehtml());

    $this->tipo      = $tipo;
    $this->id_idioma = $id_idioma;

    $w_idioma = ($id_idioma == null)?"id_idioma is null":"id_idioma = '{$id_idioma}'";

    $this->selectfrom = "select id_opcion, id_idioma, id_paxina, tipo, nome_paxina, posicion, pai from v_site_opcion";
    $this->where = "id_opcion = {$id_opcion} and {$w_idioma}";
    $this->orderby = "creado DESC";

    $this->limit_f = 10;
  }

  public function select_idoms($id_oms = null, $id_idioma = null, $id_pax = null) {
    $this->id_idioma     = $id_idioma;
    $this->id_pax_select = $id_pax;
  }

  public function inicia_where() {}

  public function operacion(EstadoHTTP $e) {
    $evento = $e->evento();

//~ echo "xpax-2:" . $evento->html(); //* ollo ajax

    if ($this->control_fslevento($evento, "op")) {
      $_ev = explode(Escritor_html::cnome   , $evento->tipo());

      if (isset($_ev[1])) $_p = explode(Escritor_html::csubnome, $_ev[1]);
      
      switch ( $_ev[0] ) {          
        case "onclick":
          return $e->obxeto("xpax")->operacion_idoms($e, null, null, $evento->subnome(0));


        case "ba":
          return $e->obxeto("xpax")->obxeto("cpaxnova")->operacion_bpaxnova($e, $_p[0]);
          
        case "bd":
          return $e->obxeto("xpax")->obxeto("cpaxdupli")->operacion_bdupli($e, $_p[0]);
          
        case "be":
          return $e->op_editar($_p[0], $_p[1]);
          
        case "bs":
          return $e->obxeto("xpax")->obxeto("cpaxsup")->operacion_bsup($e, $_p[0], ($_p[1] == "ref"));

      }
    }


    return parent::operacion($e);
  }

  public function sublista_prototipo($id_rama) {
    return null;
  }

}

//-------------------

class XPax_loms2_ehtml extends FS_ehtml {
  public $nf = 0;

  public function __construct() {
    parent::__construct();

    $this->class_table = "";
    $this->style_table = "";
    $this->width = "100%";
    $this->cellspacing = 0;
    $this->cellpadding = 0;
    $this->border = 0;
  }

  final public function en_custodia() {
    return $this->xestor->readonly;
  }

  protected function cabeceira($df = null) {
    $this->nf = $this->rbd->numFilas();

    return "";
  }

  protected function linha_detalle($df, $f) {
    $ico  = XPax_loms_ehtml::__ico( $this->xestor->tipo );

    $loms = self::__loms($this, $f);

    Btnr_loms::config2($loms, $f, $this->xestor->tipo);

    return "<tr>
              <td width=23px align=center style='min-width: 23px;'>" . $this->__bmais_html($f['id_paxina']) . "</td>
              <td width=17px style='padding: 5px 0; min-width: 23px;'>
                <img src='{$ico}' width='17px' />
              </td>
              <td width=* style='padding-left: 7px;'>" . $loms->html() . "</td>
            </tr>";
  }

  protected function totais() {
    return "
            <tr style='color: #707070;'>
              <td colspan=7 style='padding-left: 23px;'>" . $this->__paxinador_html() . "</td>
            </tr>";
  }

  private static function __loms(XPax_loms2_ehtml $ehtml, $f) {
    $b = XPax_loms_ehtml::__etq($f['titulo'], $ehtml->xestor->tipo, $f['id_paxina']);

    return $ehtml->__fslControl($b, $f['id_paxina']);
  }
}

//**************************

final class XPax_loms2_novas extends XPax_loms2 {

  public function __construct($id_opcion, $id_idioma) {
    parent::__construct("novas", $id_opcion, $id_idioma);

    $this->selectfrom = "select * from v_novas";
  }
}

//**************************

final class XPax_loms2_novas2 extends XPax_loms2 {

  public function __construct($id_opcion, $id_idioma) {
    parent::__construct("novas2", $id_opcion, $id_idioma);

    $this->selectfrom = "select * from v_novas";
    $this->orderby    = "pos DESC";
  }
}

//**************************

final class XPax_loms2_ventas extends XPax_loms2 {

  public function __construct($id_opcion, $id_idioma) {
    parent::__construct("ventas", $id_opcion, $id_idioma);

    $this->selectfrom = "select * from v_ventas";
  }
}

