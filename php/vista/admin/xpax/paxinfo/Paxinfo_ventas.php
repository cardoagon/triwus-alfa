<?php

class Paxinfo_ventas extends Paxinfo {
  public function __construct(Paxina_ventas_obd $p) {
    parent::__construct($p);
  }

  public function titulo_ficha() {
    return "Propiedades de la página de ventas";
  }

  public function paxina_obd(FS_cbd $cbd = null) {
    return $this->paxina_obd_base("ventas", $cbd);
  }
}

//----------------------------------------------

final class Paxinfo_venta extends Paxinfo {
  public function __construct(Paxina_ventas_obd $p) {
    parent::__construct($p);

    //~ $this->control_action = false;
  }

  public function titulo_ficha() {
    return "Noticia";
  }

  public function paxina_obd(FS_cbd $cbd = null) {
    return $this->paxina_obd_base("ventas", $cbd);
  }
}

