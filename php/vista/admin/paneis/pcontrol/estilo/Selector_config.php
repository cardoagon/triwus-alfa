<?php

/*************************************************

    Triwus Framework v.0

    Selector_config.php

    Author: Carlos Domingo Arias González

    Copyright (C) 2011

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation version 3.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************/

final class CPlantilla extends    Componente
                       implements ICPC       {

  public $id_usuario;
  public $id_site;
  public $id_config;

  public function __construct(FGS_usuario $u) {
    parent::__construct("cplantilla", "ptw/paneis/pcontrol/estilo/cplantilla.html");

    $this->pon_obxeto(new Param("pinfo_0"));

    $this->pon_usuario($u);
  }

  public function pon_usuario(FGS_usuario $u) {
    $this->id_usuario = $u->atr("id_usuario")->valor;
    $this->id_site    = $u->atr("id_site"   )->valor;

    $cbd = new FS_cbd();

    $s = $u->site_obd($cbd);

    $this->id_config = $s->atr("id_config")->valor;

    $p = $s->config_obd()->plantilla_obd($cbd);

    $this->obxeto("pinfo_0")->post( self::html_1($p) );
  }

  public function cpc_miga() {
    return "tu diseño";
  }

  private static function html_1(Plantilla_obd $p) {
    $a_info = $p->a_info();

    $p = (isset($a_info['escritorio']) )?"S&iacute;":"No";
    $m = (isset($a_info['mobil'])      )?"S&iacute;":"No";
    $c = ($a_info['id_usuario'] == null)?"S&iacute;":"No";

    // echo "<pre>" . print_r($a_info) . "</pre>";
    //$fontSize = trim(array_pop(explode(', ', $a_info['tipo'])));

    return "<div style='width:70%;display:grid; grid-template-columns: max-content 1fr;gap: 1em;'>
                <div><b>Nombre:</b></div>
                <div>" . ucfirst(strtolower($a_info['nome'])) . ", &nbsp;{$a_info['creado']}</div>
                <div><b>Tipografía:</b></div>
                <div>{$a_info['tipo']}</div>
                <div><b>Colores:</b></div>
                <div style='display:flex; flex-direction:column; gap: 0.5em;' >". self::html_bCores($a_info['cores']) . "</div>
            </div>";
  }

  private static function html_bCores($a_cores) {
    $html = "";

    foreach($a_cores as $id_config=>$a_cor) {
       $html .= " <div style='display:flex;align-items:center;gap: 1em;text-transform: uppercase;'>
                    <span style='display:inline-block;border-radius:100vmax; width:17px;height:17px;border: 1px #ccc solid;background-color: #{$a_cor[0]};'></span> #{$a_cor[0]}
                  </div>
                  <div style='display:flex;align-items:center;gap: 1em;text-transform: uppercase;'>
                    <span style='display:inline-block;border-radius:100vmax; width:17px;height:17px;border: 1px #ccc solid;background-color: #{$a_cor[1]};'></span> #{$a_cor[1]}
                  </div>
                  <div style='display:flex;align-items:center;gap: 1em;text-transform: uppercase;'>
                    <span style='display:inline-block;border-radius:100vmax; width:17px;height:17px;border: 1px #ccc solid;background-color: #{$a_cor[2]};'></span> #{$a_cor[2]}
                  </div>
                  <div style='display:flex;align-items:center;gap: 1em;text-transform: uppercase;'>
                    <span style='display:inline-block;border-radius:100vmax; width:17px;height:17px;border: 1px #ccc solid;background-color: #{$a_cor[3]};'></span> #{$a_cor[3]}
                  </div>";
    }

    return $html;
  }
}
