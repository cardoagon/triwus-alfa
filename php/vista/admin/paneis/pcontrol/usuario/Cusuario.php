<?php


final class Cusuario extends Componente implements ICPC {
  private $u = null;

  public function __construct(FGS_usuario $u) {
    parent::__construct("cusuario", "ptw/paneis/pcontrol/usuario/usuario.html");

    $this->u = $u;

    $custodia    = $u->en_custodia();
    $id_contacto = $u->atr("id_contacto")->valor;

    $cbd = new FS_cbd();
    
    $c = Contacto_obd::inicia($cbd, $id_contacto, "p");
    
    $this->pon_obxeto( new CPerfilContacto($c                      ) );
    
    //~ $this->pon_obxeto( new CAtPermiso     ($u->siteConfig_obd($cbd)) );
    $this->pon_obxeto( new CInfoCuenta    ($u->cuenta_obd    ($cbd)) );
    $this->pon_obxeto( new CServizos      ($u                      ) );
  }

  public function cpc_miga() {
    return "perfil de usuario";
  }

  public function operacion(EstadoHTTP $e) {
//~ echo $e->evento()->html();

    //~ if (($e_aux = $this->obxeto("catperm"        )->operacion($e)) != null) return $e_aux;
    if (($e_aux = $this->obxeto("cservizos"      )->operacion($e)) != null) return $e_aux;
    if (($e_aux = $this->obxeto("cinfocuenta"    )->operacion($e)) != null) return $e_aux;
    if (($e_aux = $this->obxeto("cperfilcontacto")->operacion($e)) != null) return $e_aux;


    return null;
  }
}

//**********************************************

final class CPerfilContacto extends Componente {
  private ?int $s = null;

  public function __construct(Contacto_obd $o) {
    parent::__construct("cperfilcontacto", "ptw/paneis/pcontrol/usuario/cperfilcontacto.html");

    $this->pon_obxeto( new CContacto() );

    $this->pon_obxeto( new CFormasPago($o->fpago_obd()) );

    $this->pon_obxeto( Panel_fs::__baceptar("Guardar", "Guardar contacto") );

    $this->obxeto("ccontacto")->post_contacto($o);

    $this->obxeto("bAceptar")->envia_ajax("onclick");
  }

  public function operacion(EstadoHTTP $e) {
//~ echo $e->evento()->html();

    if (($e_aux = $this->obxeto("ccontacto"   )->operacion($e)) != null) return $e_aux;

    if (($e_aux = $this->obxeto("cformas_pago")->operacion($e)) != null) return $e_aux;

    if ($this->obxeto("bAceptar")->control_evento()) return $this->operacion_baceptar($e);


    return null;
  }

  private function operacion_baceptar(Epcontrol $e) {
    $ccontacto = $this->obxeto("ccontacto");

    if (($erro = $ccontacto->validar()) != null) {
      $e->post_msx($erro);

      return $e;
    }
    
    $cbd = new FS_cbd();

    $cfp = $this->obxeto("cformas_pago")->obd($cbd);

    if (($erro = $cfp->validar()) != null) {
      $e->post_msx($erro);

      return $e;
    }
    
    

    $cbd->transaccion();

    if (!$ccontacto->update_contacto($cbd)) {
      $e->post_msx( "Error, sucedi&oacute; un error al guardar tu informaci&oacute;n de contacto." );

      $cbd->rollback();
    }

    if (!$cfp->update($cbd)) {
      $e->post_msx( "Error, sucedi&oacute; un error al guardar tus preferencias de pago." );

      $cbd->rollback();
    }
    
    $cbd->commit();
    
    $e->post_msx( "Operación realizada correctamente.", "m" );

    return $e;
  }
}

//**********************************************

final class CAtPermiso extends Componente {
  private ?int $s = null;

  public function __construct(Site_config_obd $o) {
    parent::__construct("catperm", "ptw/paneis/pcontrol/usuario/catperm.html");

    $this->s = $o->atr("id_site")->valor;

    $this->pon_obxeto( self::__ch("chat_0", $o->atr("at_0")->valor == 1, "Permitir acceso de administración."                        ) );
    $this->pon_obxeto( self::__ch("chat_1", $o->atr("at_1")->valor == 1, "Quiero recibir un email por cada acceso de administración.") );
  }

  public function operacion(EstadoHTTP $e) {
//~ echo $e->evento()->html();

    if ($this->obxeto("chat_0")->control_evento()) return $this->operacion_chat($e);
    if ($this->obxeto("chat_1")->control_evento()) return $this->operacion_chat($e);


    return null;
  }

  private function operacion_chat(Epcontrol $e) {
    $b0 = $this->obxeto("chat_0")->valor();
    $b1 = $this->obxeto("chat_1")->valor();
    

    return $this->operacion_aceptar($e, $b0, $b1);
  }

  private function operacion_aceptar(Epcontrol $e, bool $at_0, bool $at_1) {
    $cbd = new FS_cbd();


    $sc = Site_config_obd::inicia($cbd, $this->s);
    
    $sc->atr("at_0")->valor = ($at_0)?1:null;
    $sc->atr("at_1")->valor = ($at_1)?1:null;
    
    if ( $sc->update($cbd) )
      $e->post_msx("Actualizado permisos de administración", "m");
    else 
      $e->post_msx("Sucedió un error al actualizar la BD");

    
    $this->preparar_saida( $e->ajax() );
    
    return $e;
  }

  private static function __ch(string $k, bool $v, string $m):Checkbox {
    $o = new Checkbox($k, $v, $m);
    
    //~ $o->envia_ajax("onclick");
    $o->envia_submit("onclick");
    
    return $o;
  }
}

//**********************************************

final class CInfoCuenta extends Componente {
  private $id_usuario;

  public function __construct(Cuenta_obd $c) {
    parent::__construct("cinfocuenta", "ptw/paneis/pcontrol/usuario/cinfocuenta.html");

    $this->id_usuario = $c->atr("id_usuario")->valor;

    $this->pon_obxeto( new Param("pnome"         ) );
    $this->pon_obxeto( new Param("pmax_GB"       ) );
    $this->pon_obxeto( new Param("pmultilinguaxe") );
    $this->pon_obxeto( new Param("pventa"        ) );
    $this->pon_obxeto( new Param("pemails"       ) );
    $this->pon_obxeto( new Param("subs_n"        ) );
    $this->pon_obxeto( new Param("subs_v"        ) );
    $this->pon_obxeto( new Param("pdominio"      ) );
    $this->pon_obxeto( new Param("consumo_GB"    ) );

    $this->iniciar($c);
  }

  private function iniciar(Cuenta_obd $c) {
//~ echo "<pre>" . print_r($c->a_resumo(), 1) . "</pre>";

    $ml         = ($c->atr("multilinguaxe")->valor > 0)?"Sí":"No";
    $v          = ($c->atr("venta"        )->valor > 0)?"Sí":"No";
    $pmax       = $c->atr("max_GB")->valor;
    $consumo_GB = "--";

    
    //* calcula consumos.
    if (($_maxGB = $c->_maxGB()) == null) {
      $pmax = "";
      $em   = "";
      $dom  = "";
    }
    else {
      if ($_maxGB['gigas'] > 0) $pmax .= " + {$_maxGB['gigas']}";

          if (($em = $c->atr("email")->valor) == 0) $em = "Ilimitadas";
      elseif ($_maxGB['email'] > 0)                 $em .= " + {$_maxGB['email']}";
      
      if ( ($dom = $c->atr("dominio")->valor) == null ) {
        $dom  = "--";
      }
      else {
        $psi = new Plesk_siteinfo();

        $consumo_GB = File::html_bytes($psi->getinfo_realSize( $dom ));    
      }
    }


    //* calcula subs.
    $_subs  = $c->_subs();
    
    $subs_n = null;
    $subs_v = null;
    foreach ($_subs as $_s) {
      $subs_n .= "<div>{$_s["nome"   ]}</div>";
      $subs_v .= "<div>{$_s["version"]}</div>";
    }
    
    if ( $subs_n == null ) $subs_n = "<div>--</div>";
    if ( $subs_v == null ) $subs_v = "<div>--</div>";


    //* escribe datos.
    $this->obxeto( "pnome"          )->post( $c->atr("nome")->valor );
    $this->obxeto( "pmax_GB"        )->post( $pmax                  );
    $this->obxeto( "pmultilinguaxe" )->post( $ml                    );
    $this->obxeto( "pventa"         )->post( $v                     );
    $this->obxeto( "pemails"        )->post( $em                    );
    $this->obxeto( "subs_n"         )->post( $subs_n                );
    $this->obxeto( "subs_v"         )->post( $subs_v                );
    $this->obxeto( "pdominio"       )->post( $dom                   );
    $this->obxeto( "consumo_GB"     )->post( $consumo_GB            );
  }
}

