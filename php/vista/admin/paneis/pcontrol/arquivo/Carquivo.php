<?php


final class CArquivo extends Componente implements ICPC {
  const ptw_0  = "ptw/paneis/pcontrol/arquivo/carquivo.html";
  //~ const ptw_1a = "ptw/paneis/pcontrol/arquivo/carquivo1a.html";
  //~ const ptw_1b = "ptw/paneis/pcontrol/arquivo/carquivo1b.html";
  const ptw_2  = "ptw/paneis/pcontrol/arquivo/carquivo2.html";
  const ptw_3  = "ptw/paneis/pcontrol/arquivo/carquivo3.html";

  const max_megas = 25;

  public $id_site   = null;
  public $protocolo = "https://";
  public $dominio   = null;

  public function __construct(FGS_usuario $u) {
    parent::__construct("carquivo", self::ptw_0);

    $src_tmp  = self::__src_inicia($u, 4);
    $src_pub  = self::__src_inicia($u, 1);
    $src_priv = self::__src_inicia($u, 2);


    $this->id_site = $u->atr("id_site")->valor;

    $this->pon_obxeto(new Select("sUbicacion", array("1"=>"Carpeta pública", "2"=>"Carpeta privada")));

    $this->pon_obxeto(self::__bAdd());
    $this->pon_obxeto(self::__fAdd($src_tmp));
    $this->pon_obxeto(self::__bAdd2());

    $this->pon_obxeto(new CArquivo_novaCarpeta());

    $this->pon_obxeto( new FSL_arquivo("ls"  , $src_pub        ) );
    $this->pon_obxeto( new FSL_arquivo("ls_2", $src_priv, false) );

    $this->pon_obxeto(Panel_fs::__espera());

    $this->obxeto("sUbicacion" )->post("1");
    $this->obxeto("sUbicacion" )->envia_submit("onchange");
    //~ $this->obxeto("sUbicacion" )->envia_ajax("onchange");

    //* outras configuracions
    $cbd = new FS_cbd();

    $sp = Site_plesk_obd::inicia_s($cbd, $this->id_site);

    $this->dominio = $sp->atr("dominio")->valor;

    $this->protocolo = "https://";
  }

  public function cpc_miga() {
    return "Triwus";
  }

  public function readonly($b = true) {
    $this->obxeto("bAdd" )->visible = !$b;
    $this->obxeto("bAdd2")->visible = !$b;

    $this->obxeto("ls"  )->readonly($b);
    $this->obxeto("ls_2")->readonly($b);
  }

  public function operacion(EstadoHTTP $e) {
//~ echo $e->evento()->html();

    if (($e_aux = $this->operacion_fAdd($e)) != null) return $e_aux;

    if (($e_aux = $this->obxeto("ls"  )->operacion($e)) != null) return $e_aux;
    if (($e_aux = $this->obxeto("ls_2")->operacion($e)) != null) return $e_aux;


    if (($e_aux = $this->obxeto("carquivo_nc")->operacion($e)) != null) return $e_aux;

    if ($this->obxeto("bAdd2")->control_evento()) return $this->operacion_bAdd2($e);


    if ($this->obxeto("sUbicacion")->control_evento()) $this->operacion_sUbicacion($e);


    return parent::operacion($e);
  }

  public function __src_publica() {
    return ($this->obxeto("sUbicacion")->valor() != 2);
  }

  public function __src($base64 = false, $completa = true) {
    if (!$this->__src_publica()) return $this->obxeto("ls_2")->__src($base64);

    $src_0 = $this->obxeto("ls")->__src();


    if ($this->dominio == null        ) return $src_0;

    if (!$completa)                     return $src_0;

    $a_src_0 = explode("/", $src_0);

    $src = "";
    for ($i = 2; $i < count($a_src_0); $i++) $src .= "/{$a_src_0[$i]}";


    return "{$this->protocolo}{$this->dominio}{$src}";
  }

  public static function __src_inicia(FGS_usuario $u, $tipo) {
    $s = $u->atr("id_site")->valor;

    switch ($tipo) {
      case 1: return Efs::url_site($s) . Refs::url_arquivo . "/";
      case 2: return Efs::url_arquivo_priv($s);
      case 4:  {
        $tmp = Efs::url_site($s) . Refs::url_arquivos . "/tmp/";

        if (!is_dir($tmp)) mkdir($tmp);

        return $tmp;
      }
    }
  }

  public function operacion_sUbicacion(Epcontrol $e) {
    $u = $this->obxeto("sUbicacion")->valor();

    if ($u == 1) {
      $this->ptw = self::ptw_0;
    }
    elseif ($u == 2) {
      $this->ptw = self::ptw_2;
    }


    return $e;
  }

  private function operacion_bAdd2(Epcontrol $e) {
    $onc = $this->obxeto("carquivo_nc");

    $onc->abrir();

    $onc->preparar_saida($e->ajax());

    return $e;
  }

  private function operacion_fAdd(Epcontrol $e) {
    if (!$this->obxeto("fAdd")->control_evento()) return null;
    
    try {
      $this->obxeto("fAdd")->post_f(false);


      if ($this->obxeto("sUbicacion")->valor() == 1)
        $src = $this->obxeto("ls"  )->__src();
      else
        $src = $this->obxeto("ls_2")->__src();

      $this->obxeto("fAdd")->aceptar_f($src);
    }
    catch (Exception $ex) {
      $e->post_msx($ex->getMessage());
      
      return $e;
    }

    $this->preparar_saida($e->ajax());

    return $e;
  }

  private static function __bAdd() {
    $b = Panel_fs::__bmais("bAdd", "Archivo", "Añadir un fichero");

    $b->pon_eventos("onclick", "pcontrol_arquivo_bAdd_click(this)");

    return $b;
  }

  private static function __bAdd2() {
    $b = Panel_fs::__bmais("bAdd2", "Carpeta", "Añadir una carpeta");

    $b->envia_ajax("onclick");

    return $b;
  }

  private static function __fAdd($src_tmp) {
    $f = new File("fAdd", $src_tmp, 1024 * 1024 * self::max_megas);

    $f->clase_css("default", "file_apagado");

    return $f;
  }
}

//******************************************

final class CArquivo_novaCarpeta extends Componente {
  const ptw = "ptw/paneis/pcontrol/arquivo/carquivo_nc.html";

  private static $_charval = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_0123456789";

  public function __construct() {
    parent::__construct("carquivo_nc");

    $this->pon_obxeto(Panel_fs::__text("tCarpeta"));

    $this->pon_obxeto(Panel_fs::__baceptar ());
    $this->pon_obxeto(Panel_fs::__bcancelar());

    $this->obxeto("bCancelar")->envia_ajax("onclick");
    $this->obxeto("bAceptar" )->envia_ajax("onclick");
  }

  public  function abrir() {
    $this->obxeto("tCarpeta")->post("");

    $this->ptw = self::ptw;
  }

  public  function pechar() {
    $this->ptw = null;
  }

  public  function operacion(EstadoHTTP $e) {
    if ($this->obxeto("bAceptar" )->control_evento()) return $this->operacion_bAceptar ($e);
    if ($this->obxeto("bCancelar")->control_evento()) return $this->operacion_bCancelar($e);

    return null;
  }



  private function operacion_bCancelar(Epcontrol $e) {
    $this->pechar();

    $this->preparar_saida($e->ajax());

    return $e;
  }

  private function operacion_bAceptar (Epcontrol $e) {
    if (($erro = $this->validar()) != null) {
      $e->post_msx($erro);

      return $e;
    }


    $e->post_msx("Carpeta creada correctamente.", "m");

    $this->pechar();

    $this->pai->preparar_saida($e->ajax());

    return $e;
  }

  private function validar() {
    if (($c = self::validar_caracteres( $this->obxeto("tCarpeta")->valor() )) == null) return "Nombre de carpeta no válido.";

    $src     = $this->pai->__src(false, false) . $c;

    if ( is_file($src) ) return "El nombre: {$c}, ya está usado por un archivo.";

    if ( is_dir ($src) ) return "El nombre: {$c}, ya está usado por otra carpeta.";

    if ( !mkdir ($src) ) return "Error: CArquivo_novaCarpeta.validar(). ( !mkdir(\"{$src}\") ).";

    return null;
  }

  private static function validar_caracteres(string $s):?string {
    if (($s = trim( $s )) == "") return null;
    
    $s = str_replace(" ", "_", $s);
    
    $s2 = "";
    for ($i = 0; $i < strlen($s); $i++) {
      if (strpos(self::$_charval, $s[$i]) === false) continue;
      
      $s2 .= $s[$i];
    }

    if ($s2 == "") return null;
    
    
    return $s2;
  }
}


//******************************************


class FSL_arquivo extends FS_lista {
  const bSubir = "<div style='transform: rotate(180deg);display: inline-block;'>↴</div> ..";

  public $readonly = false;
  public $pub      = true;

  protected $raiz = null;
  protected $src  = null;

  public function __construct($id, $src, $pub = true) {
    parent::__construct($id, new FSehtml_arquivo());

    $this->pub  = $pub;
    $this->src  = $src;
    $this->raiz = $src;

    $this->pon_obxeto( new Hidden("hmsx") );

    $this->pon_obxeto( Panel_fs::__bapagado("bSubir", null, null, false, self::bSubir) );

    $this->obxeto("bSubir")->style("default", "margin-right: 2em; color: #00f;");

    $this->obxeto("bSubir")->envia_submit("onclick");
  }

  public function readonly($b = true) {
    $this->readonly = $b;
  }

  public function __count() {
    return $this->ehtml->numFilas();
  }

  public function __src($base64 = false) {
    if ($base64) return base64_encode($this->src);

    return $this->src;
  }

  public function __src_html() {
    $_d = explode( "/", substr( $this->src, strlen($this->raiz) ) );
//~ echo "<pre>" . print_r($_d, 1) . "</pre>";

    $ct_d = count($_d);


    $p = ($this->pub)?"pública":"privada";

    $p = self::_lsrc($p, "0", $ct_d > 1);

    $d  = "";
    for ( $i = 0; $i < ($ct_d - 1); $i++ ) {
      if ($i > 0) $d .= "/";

      $d .= self::_lsrc( $_d[$i], $i + 1, $i < ($ct_d - 2) );
    }

    return "<div class='pcontrol_docs_src_0'>{$p}://{$d}</div>";
  }

  public function __raiz() {
    return $this->src == $this->raiz;
  }

  public function operacion(EstadoHTTP $e) {
    if ( $this->obxeto("bSubir")->control_evento() ) return $this->operacion_ver($e, -1);

    $evento = $e->evento();

    if ($this->control_fslevento($evento, "tnome"      )) return $this->operacion_tnome    ($e, $evento->subnome(0));
    if ($this->control_fslevento($evento, "baixar_priv")) return $this->operacion_descargar($e, $evento->subnome(0));
    if ($this->control_fslevento($evento, "ver"        )) return $this->operacion_ver      ($e, $evento->subnome(0));
    if ($this->control_fslevento($evento, "lsrc"       )) return $this->operacion_lsrc     ($e, $evento->subnome(0));
    if ($this->control_fslevento($evento, "borrar"     )) return $this->operacion_borrar   ($e, $evento->subnome(0));
    if ($this->control_fslevento($evento, "descargar"  )) return $this->operacion_descargar($e, $evento->subnome(0));


    return parent::operacion($e);
  }

  public function operacion_tnome(Epcontrol $e, $i) {
    if ($i == 0) $i = "0";

    $f = $this->ehtml->iterador()->fila($i);

    $src_0 = "{$this->src}{$f['Nombre']}";
    $src_f = $this->src . $this->obxeto("hmsx")->valor();


    rename($src_0, $src_f);

    $this->preparar_saida($e->ajax());

    return $e;
  }

  public function operacion_descargar(Epcontrol $e, $i) {
    if ($i == 0) $i = "0";

    $f = $this->ehtml->iterador()->fila($i);

    $src = "{$this->src}{$f['Nombre']}";

    if (strtolower($f['Tipo']) != "directory") {
      $e->pon_mime($f['Nombre'], $f['Tipo'], file_get_contents($src), filesize($src));

      return $e;
    }

    $fzip    = md5($src) . ".zip";

    $src_tmp = "tmp/{$fzip}";

    //~ echo "{$src}::{$src_tmp}<br />";

    XestorDescargas::borra_antigos("tmp", 180);

    Zip_carpeta::zip($src, $src_tmp);

    //~ $e->pon_mime($fzip, filetype($src_tmp), file_get_contents($src_tmp), filesize($src_tmp));

    $e->redirect($src_tmp);

    return $e;
  }

  public function operacion_ver(Epcontrol $e, $i) {
    if ($i == 0) $i = "0";

    if ($i == -1) {
      $this->__src_anterior();
    }
    else {
      $f = $this->ehtml->iterador()->fila($i);

      $this->src .= "{$f['Nombre']}/";
    }

    $this->src = str_replace("//", "/", $this->src);


    return $e;
  }

  public function operacion_lsrc(Epcontrol $e, $i) {
//~ echo "11111111111111" .  $e->evento()->html();

    $_src = explode( "/", substr( $this->src, strlen($this->raiz) ) );

//~ echo "0::{$this->src}<br>";

    $this->src = $this->raiz;
    for ( $j = 0; ( ($j < $i) && ($j < (count($_src) - 1)) ); $j++ ) $this->src .= "{$_src[$j]}/";

//~ echo "1::{$this->src}<br>";


    return $e;
  }

  public function operacion_borrar(Epcontrol $e, $i) {
    if ($i == 0) $i = "0";

    $f = $this->ehtml->iterador()->fila($i);

    $src = "{$this->src}{$f['Nombre']}";


    if (strtolower($f['Tipo']) == "directory") exec("rm -r \"{$src}\"");
    else                                       unlink($src);


    $this->pai->preparar_saida($e->ajax());


    return $e;
  }

  protected function html_baleiro() {
    if (!$this->visible) return "";

    $html_subir = ($this->__raiz())?"":$this->obxeto("bSubir")->html();

    return $this->__src_html() . "
           <div class='txt_adv'>{$html_subir}La carpeta está vacía.</div>";
  }

  protected function __iterador() {
    $it = new CArquivo_iterador($this->src, $this->orderby);

    if ($this->orderby == null) $this->orderby = "Nombre ASC";

    return $it;
  }

  private function __src_anterior() {
    $a_src = explode("/", $this->src);

    if (count($a_src) < 2) return;

    $src_aux = "";
    for($i = 0; $i < (count($a_src) - 2); $i++) $src_aux .= $a_src[$i] . "/";
    
    if (strlen($src_aux) >= strlen($this->raiz)) $this->src = $src_aux;
  }

  private function _lsrc($n, $i, $onclick) {
    $k = $this->nome_completo() . Escritor_html::cnome . "lsrc" . Escritor_html::csubnome . $i;

    $l = new Div($k, $n);

    if ($onclick) $l->envia_SUBMIT("onclick");

    return $l->html();
  }
}

//------------------------------------------------------

final class CArquivo_iterador implements Iterador_bd {
  private $filtro_1 = false; //* para empregar na carpeta arquivos, filtra as diferentes versions dunha imaxe.

  private $src  = null;

  private $a    = null;
  private $i_a  = null;
  private $i    = -1;

  public function __construct($src, $orderby, $filtro_1 = false) {
    $this->src  = $src;
    $this->a    = null;

    $this->filtro_1 = $filtro_1;


    $finfo = finfo_open(FILEINFO_MIME_TYPE); // devuelve el tipo mime de su extensión


    if (($a_glob = glob("{$src}*")) == null) return;

    foreach($a_glob as $g) {
      $g2 = basename($g);

      if ($g2 == ".")  continue;
      if ($g2 == "..") continue;

      if ($this->filtro_1($g2)) continue;

      $g3 = "{$src}{$g2}";

      $i = (is_array($this->a))?count($this->a):0;

      $a_i = array("i"=>$i, "Nombre"=>$g2, "Tamaño"=>filesize($g3), "Tipo"=>finfo_file($finfo, $g3), "Creado"=>filemtime($g3));

      $this->a[$i] = $a_i;
    }

    finfo_close($finfo);

    $this->__orde($orderby);
  }

  public function fila($i) {
    return $this->a[$i];
  }

  public function numFilas() {
    if ( !is_array($this->a) ) return 0;

    return count($this->a);
  }

  public function descFila() {}

  public function next() {
    $this->i++;

    if ($this->i >= count($this->a)) {
      $this->i = -1;

      return null;
    }

    return $this->a[ $this->i_a[$this->i] ];
  }

  private function __orde($orderby) {
    if ($orderby == null) $orderby = "";
    
    if (strpos($orderby, " ") == FALSE) {
      $o1 = $orderby;
      $o2 = null;
    }
    else
      list($o1, $o2) = explode(" ", $orderby);

    for ($i = 0; $i < count($this->a); $i++) {
      if ($o1 == null)
        $this->i_a[] = $i;
      else
        $this->i_a[] = $this->a[$i][$o1];
    }

    if (count($this->i_a) == 0) {
      return;
    }

    if ($orderby == null) {
      return;
    }

    if ($o2 == "ASC")
      asort($this->i_a);
    else
      arsort($this->i_a);


    $i = 0;
    foreach ($this->i_a as $k=>$v) {
      $this->i_a[$i] = $k;

      $i++;
    }
  }

  private function filtro_1($s) {
    if (!$this->filtro_1) return false;

    if (strpos($s, ".") === FALSE) return false;

    list($url0, $w) = explode(".", $s);

    return $w != null;
  }
}

//------------------------------------------------------

final class FSehtml_arquivo extends FS_ehtml {
  public $nf = 0;

  public function __construct() {
    parent::__construct();

    $this->class_table = "lista_00";
    $this->style_table = "border: 1px solid #e6e6e6;margin-top: 27px;";
    $this->align       = "center";
    $this->width       = "100%";
    $this->cellspacing = 0;
    $this->cellpadding = 3;
    $this->border      = 0;
  }

  public function iterador() {
    return $this->rbd;
  }

  public function numFilas() {
    return $this->iterador()->numFilas();
  }

  public function html_baleiro() {
    return "<table style='padding: 17px 32px 17px 17px;' align=center border=0 cellpadding=3 cellspacing=0 width=100% >
            " . $this->cabeceira() . "
           </table>";
  }

  protected function cabeceira($df = null) {
    $tr0 = "<tr style='border: 0;'>
            <td colspan=11 >" . $this->xestor->__src_html() . "</td>
           </tr>";

    $tr2 = "";
    if (!$this->xestor->__raiz()) {
      $tr2 = "<tr  >
              <td>" . self::__mime($this, 'directory') . "</td>
              <td>" . self::__bvercarpeta($this, -1, FSL_arquivo::bSubir) . "</td>
              <td width=1px>directory</td>
              <td width=1px></td>
              <td width=1px></td>
              <td width=1px></td>
              <td width=1px></td>
              <td width=1px></td>
            </tr>";
    }

    return $this->xestor->obxeto("hmsx")->html() . "
           {$tr0}
           <tr>
             <th width=23px></th>
             <th>" . $this->__lOrdenar_html("Nombre") . "</th>
             <th width=1px>" . $this->__lOrdenar_html("Tipo") . "</th>
             <th width=1px align=right>" . $this->__lOrdenar_html("Tamaño") . "</th>
             <th width=1px>" . $this->__lOrdenar_html("Modificado") . "</th>
             <th width=37px></th>
             <th width=37px></th>
             <th width=37px></th>
           </tr>" . $tr2;
  }

  protected function linha_detalle($df, $f) {
    $i    = $f['i'];

    list($tm, $tu)  = explode(" ", File::html_bytes($f["Tamaño"]));

    if ($f['Tipo'] == "directory") {
      $tm = "";
      $tu = "";
    }
    else
      $tm = number_format($tm, 1, ",", ".");

    $creado = str_replace(" ", "&nbsp;", date("d-m-Y H:i:s", $f["Creado"]));

    return "<tr>
              <td>" . self::__mime($this, $f['Tipo']) . "</td>
              <td>&nbsp;" . self::__n($this, $f) . "</td>
              <td>{$f['Tipo']}</td>
              <td align=right>{$tm}&nbsp;{$tu}</td>
              <td>{$creado}</td>
              <td>" . self::__benlace($this, $f) . "</td>
              <td>" . self::__bdescargar($this, $i) . "</td>
              <td>" . self::__bborrar($this, $i) . "</td>
            </tr>";
  }

  protected function totais() {
    return "";
  }

  private static function __mime(FSehtml_arquivo $ehtml, $m) {
    if     (strpos($m, "audio")     !== FALSE) $m2 = Ico::mime_audio;
    elseif (strpos($m, "image")     !== FALSE) $m2 = Ico::mime_image;
    elseif (strpos($m, "directory") !== FALSE) $m2 = Ico::mime_folder;
    elseif (strpos($m, "video")     !== FALSE) $m2 = Ico::mime_video;
    elseif (strpos($m, "zip")       !== FALSE) $m2 = Ico::mime_package;
    elseif (strpos($m, "x-tar")     !== FALSE) $m2 = Ico::mime_package;
    elseif (strpos($m, "package")   !== FALSE) $m2 = Ico::mime_package;
    elseif (strpos($m, "pdf")       !== FALSE) $m2 = Ico::mime_pdf;
    else                                       $m2 = Ico::mime_text;


    return "<img src='{$m2}' width=22px height=22px />";
  }

  private static function __sep() {
    return "<tr height=1px><td colspan=9 style='font-size: 1px; border-top: 1px solid #e6e6e6;'>&nbsp;</td></tr>";
  }

  private static function __n(FSehtml_arquivo $ehtml, $f) {
    if ($f['Tipo'] == "directory") return self::__bvercarpeta($ehtml, $f['i'], $f['Nombre']);

    $t = new Text("tnome");

    $t->post($f['Nombre']);

    $t->maxlength = 255;

    $t->style("default", "border: 0;border-bottom: 1px solid #333;background-color: #eee;width: 93%;");

    $t->pon_eventos("onchange", "carquivo_tnome(this, {$f['i']})");


    return $ehtml->__fslControl($t, $f['i'])->html();
  }

  private static function __benlace(FSehtml_arquivo $ehtml, $f) {
    if (!$ehtml->xestor->pub     ) return "";

    if ($f['Tipo'] == "directory") return "";

    $href = $ehtml->xestor->pai->__src() . $f['Nombre'];

    $b = Panel_fs::__bapagado("enlace", Ico::blink2, "Copiar enlace", "19", "");

    $b->pon_eventos("onclick", "carquivo_benlace('{$href}')");


    return $b->html();
  }

  private static function __bdescargar(FSehtml_arquivo $ehtml, $id) {
    $b = Panel_fs::__bapagado("descargar", Ico::bdescargar, "Descargar entrada", "19", "");

    $b->envia_submit("onclick");
    //~ $b->envia_MIME("onclick");

    return $ehtml->__fslControl($b, $id)->html();
  }

  private static function __bborrar(FSehtml_arquivo $ehtml, $id) {
    if ($ehtml->xestor->readonly) return "";

    $b = Panel_fs::__bapagado("borrar", Ico::btrash, "Eliminar entrada", false, "");

    $b->envia_ajax("onclick");

    return $ehtml->__fslControl($b, $id)->html();
  }

  private static function __bvercarpeta(FSehtml_arquivo $ehtml, $id, $txt) {
    $b = Panel_fs::__bapagado("ver", null, null, false, $txt);


    //~ $b->envia_ajax("onclick");


    return $ehtml->__fslControl($b, $id)->html();
  }

}


//******************************************

final class CArquivo_descarga extends Componente {
  //~ const ptw_0 = "ptw/paneis/pcontrol/arquivo/carquivo_descarga_0.html";
  
  private Proc2P $p2p;

  public function __construct(FGS_usuario $u) {
    parent::__construct("carquivo_descarga");
    
    $this->p2p = new Proc2P( new TRWZIP(), $u->atr("id_usuario")->valor );
  }

  private static function _p2p(FGS_usuario $u):Proc2P {
    return new Proc2P( new TRWZIP(), $u->atr("id_usuario")->valor );
  }
}
