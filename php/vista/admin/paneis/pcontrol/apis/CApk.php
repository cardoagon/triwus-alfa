<?php

final class CApk extends Componente implements ICPC, IXestorCEditor_imx {
  //~ const ptw_0 = "ptw/paneis/pcontrol/androidAPK/android.html";
  const ptw_1 = "ptw/paneis/pcontrol/androidAPK/android-1.html";
  
  const url_apk = "ptw/paneis/pcontrol/androidAPK/APK/triwus.apk";

  private $id_site = null;
  private $ico_obd = null;

  public function __construct(FGS_usuario $u, string $id_site) {
    parent::__construct("capk", self::ptw_1);
    
    $this->pon_obxeto(new Text("apk_dominio"));
    $this->pon_obxeto(new Text("apk_nombre"));
    $this->pon_obxeto(new Text("apk_nombreEmpresa"));
    $this->pon_obxeto(new Text("apk_ciudad"));
    $this->pon_obxeto(new Text("apk_provincia"));
    $this->pon_obxeto(new Text("apk_codigoPais"));

    $this->pon_obxeto( Panel_fs::__baceptar());      // [bAceptar]
    
    $cbd = new FS_cbd();
    $r = Site_obd::inicia($cbd, $id_site)->metatags_obd($cbd);
    echo "<pre>" . print_r($r->atr("id_logo")->valor(),1) . "</pre>";
    
    $this->id_site = $id_site;
    $this->ico_obd = $r->__ico($cbd);

    $this->pon_obxeto(CMetatags::__bico($this->ico_obd));
    $this->pon_obxeto(CMetatags::__fico($this->id_site));
    $this->pon_obxeto(CMetatags::__mico());
    $this->pon_obxeto(new CEditor_imx());
    $this->pon_obxeto(self::apk_logo());
    $this->__inicia($u);
    
    $url = null; if (($l = $r->iclogo_obd($cbd)) != null) $url = $l->url();

    $this->post_url($url);

    /*
      $this->pon_obxeto( Panel_fs::__bexportar("bapk", "Descargar APK", "Pincha aquí para descargar una APK"));
      
      $this->obxeto("bapk")->envia_mime("onclick");
    */  }

  public function operacion(EstadoHTTP $e) {
    //echo $e->evento()->html();
    $this->obxeto("mico")->visible = false;


    if ($this->obxeto("logo")->control_evento()) return $this->obxeto("ceditor_imx")->operacion_logo($e);
    if (($e_aux = $this->obxeto("ceditor_imx")->operacion($e)) != null) return $e_aux;
    
    if ($this->obxeto("bAceptar" )->control_evento()) return $this->operacion_baceptar($e);

    //~ if ($this->obxeto("bapk")->control_evento()) return $this->operacion_bapk ($e);
    if ($this->obxeto("bAceptar")->control_evento()) {
      $this->update( new FS_cbd() );

      return $e;
    } 

    return null;
  }

  private function operacion_bapk(Epcontrol $e) {
    //~ echo "procesamos el evento<br>";

      //~ $d = Refs::url_tmp2 . $this->id_site; //* url_tmp
      
    //~ echo getcwd();
        
    //~ echo is_file(self::url_apk)?"11111":"222222";
    //~ echo mime_content_type(self::url_apk);

      //~ $e->pon_mime("apk-{$this->id_site}.apk", mime_content_type(self::url_apk), file_get_contents(self::url_apk), null);



      //~ exec('cd /var/www/html/ptw/paneis/pcontrol/andoidAPK/APK');

      //~ echo exec('ls -l');

      /* //Comprimimos la apk en un zip
      exec('zip /var/www/dominio/triwus/ptw/paneis/pcontrol/androidAPK/APK/triwus.zip /var/www/dominio/triwus/ptw/paneis/pcontrol/androidAPK/APK/triwus.apk');

      //descargarmos el zip, comando unzip para descomprimir
      exec('wget http://localhost/triwus/ptw/paneis/pcontrol/androidAPK/APK/triwus.zip');

      //Eliminar el zip que se genero para la descarga
      exec('rm /var/www/dominio/triwus/ptw/paneis/pcontrol/androidAPK/APK/triwus.zip'); */

    return $e;
  }

  private function operacion_baceptar(Epcontrol $e) {
    //echo "procesamos el evento: bAceptar.<br>";

    
    if (($erro = $this->__validar()) != null) {
      $e->post_msx($erro);

      return $e;
    }

    /*
        if ($this->obxeto("apk_dominio")->valor() != null) {
          $e->post_msx( $this->obxeto("apk_dominio")->valor() );
          
          return $e;
        }
        if ($this->obxeto("apk_nombre")->valor() != null) {
          $e->post_msx( $this->obxeto("apk_nombre")->valor() );
          
          return $e;
        }

        if ($this->obxeto("apk_apellidos")->valor() != null) {
          $e->post_msx( $this->obxeto("apk_apellidos")->valor() );
          
          return $e;
        }
        if ($this->obxeto("apk_nombreEmpresa")->valor() != null) {
          $e->post_msx( $this->obxeto("apk_nombreEmpresa")->valor() );
          
          return $e;
        }
        if ($this->obxeto("apk_ciudad")->valor() != null) {
          $e->post_msx( $this->obxeto("apk_ciudad")->valor() );
          
          return $e;
        }
        if ($this->obxeto("apk_provincia")->valor() != null) {
          $e->post_msx( $this->obxeto("apk_provincia")->valor() );
          
          return $e;
        } 
        if ($this->obxeto("apk_codigoPais")->valor() != null) {
          $e->post_msx( $this->obxeto("apk_codigoPais")->valor() );
          
          return $e;
        } 

        $this->ptw = self::ptw_1;
    */


    $e->post_msx( "okok", "m" );

    echo "<pre>" . print_r($this->cuerpoMensaje(), 1) . "</pre>";

    $this->__enviarMensaje();

    return $e;
  }

  private function __validar():?string {
    $erro = null;

    if ( trim($this->obxeto("apk_dominio")       ->valor()) == "" ) $erro .= "Debes teclear un dominio.<br>";
    if ( trim($this->obxeto("apk_nombre")        ->valor()) == "" ) $erro .= "Debes teclear el nombre.<br>";
    if ( trim($this->obxeto("apk_nombreEmpresa") ->valor()) == "" ) $erro .= "Debes teclear el nombre de la empresa.<br>";
    if ( trim($this->obxeto("apk_ciudad")        ->valor()) == "" ) $erro .= "Debes teclear una ciudad.<br>";
    if ( trim($this->obxeto("apk_provincia")     ->valor()) == "" ) $erro .= "Debes teclear una privincia.<br>";
    if ( trim($this->obxeto("apk_codigoPais")    ->valor()) == "" ) $erro .= "Debes teclear el código de un pais.<br>";

    return $erro;
    
  }

  private function __inicia(FGS_usuario $u):void {
//echo "<pre>" . print_r($u->a_resumo(), 1) . "</pre>";ssss

    $c = $u->contacto_obd(); 
    
//echo "<pre>" . print_r($c->a_resumo(), 1) . "</pre>";

    $e = $c->enderezo_obd();

//echo "prov::" .$e->prov() . "<br>";


    $s = Site_obd::inicia(new FS_cbd(), $this->id_site);

//echo "<pre>" . print_r($s->a_resumo(), 1) . "</pre>";


    //~ $c = $u->contacto_obd();

    $this->obxeto("apk_dominio"      )->post($s->atr("dominio"     )->valor);
    $this->obxeto("apk_nombre"       )->post($u->atr("nome"        )->valor);
    $this->obxeto("apk_nombreEmpresa")->post($u->atr("razon_social")->valor);
    $this->obxeto("apk_ciudad"       )->post($e->atr("localidade"  )->valor);
    $this->obxeto("apk_provincia"    )->post($e->prov());
    $this->obxeto("apk_codigoPais"   )->post("ES");
    
  }
 
  private function __enviarMensaje():?string {
    $m = new XMail3($this->id_site);

    $_x = $m->_config();

    try {
      $m->pon_rmtnt($_x["email"]);
      
      $m->pon_direccion("info@triwus.com");  
      $m->pon_cc($_x["email"]); 

      $m->pon_asunto("Datos apk");
      
      $m->pon_corpo("Datos del formulario: <br><br>" . $this->cuerpoMensaje() . "<br>" . date("Y-m-d H:i:s") . ".");
      
      //~ $m->pon_adxunto($this->obxeto("ceditor_imx")->aceptar_imx());
      $m->pon_adxunto($this->url);

      $m->enviar();
    }
    catch (Exception $exc) {
      $msx = "Cliente SMTP KO:<br><br>" . $exc->getMessage();
      
      return $msx;
    }
    
    return null;
  }

  private function cuerpoMensaje(){

    $cuerpo  = "Dominio: "   . $this->obxeto("apk_dominio"      )->valor() . "<br>";
    $cuerpo .= "Nombre: "    . $this->obxeto("apk_nombre"       )->valor() . "<br>";
    $cuerpo .= "Empresa: "   . $this->obxeto("apk_nombreEmpresa")->valor() . "<br>";
    $cuerpo .= "Ciudad: "    . $this->obxeto("apk_ciudad"       )->valor() . "<br>";
    $cuerpo .= "Provincia: " . $this->obxeto("apk_provincia"    )->valor() . "<br>";
    $cuerpo .= "País: "      . $this->obxeto("apk_codigoPais"   )->valor() . "<br>";

    return $cuerpo;
  }

  public function cpc_miga() {
    return "Android";
  }


  public function url() {
    return $this->url;
  } 

  public function post_url($url = null) {

    $this->url = $this->obxeto("ceditor_imx")->pon_src($url);
    

    $this->obxeto("logo")->style("default" , "background-image: url({$this->url});cursor: pointer;");
    $this->obxeto("logo")->style("readonly", "background-image: url({$this->url});");

  }

  
  public function update(FS_cbd $cbd) {
    $r = Site_metatags_obd::inicia($cbd, $this->id_site);

    $r->atr("titulo_ico")->valor  = $this->ico_obd->atr("id_elmt")->valor;
    $r->atr("titulo_txt")->valor  = $this->obxeto("titulo")->valor();

    if (!$r->update($cbd)) return false;
    
    return $this->update_logo($cbd, $r);
  }
  
  private function update_logo(FS_cbd $cbd, Site_metatags_obd $r) {
    if (($url = $this->obxeto("ceditor_imx")->aceptar_imx()) == null) return true;


    return $r->iclogo_update($cbd, $url);
  }

  
  private static function apk_logo() {
    $l = new Div("logo");
  
    $l->envia_AJAX("onclick");
  
    $l->clase_css("default", "paxinfo_logo");
  
    return $l;
  }
  
}
