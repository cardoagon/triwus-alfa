<?php

final class CMail extends Componente implements ICPC {
  const ptw_0 = "ptw/paneis/pcontrol/mail/cmail_0.html";
  const ptw_1 = "ptw/paneis/pcontrol/mail/cmail_1.html";
  const ptw_2 = "ptw/paneis/pcontrol/mail/cmail_2.html";
  const ptw_3 = "ptw/paneis/pcontrol/mail/cmail_3.html";
  const ptw_4 = "ptw/paneis/pcontrol/mail/cmail_4.html";
  const ptw_5 = "ptw/paneis/pcontrol/mail/cmail_5.html";

  public $err_internet = false;

  private $id_site  = null;
  private $id_plesk = null;
  private $dominio  = null;

  private $email_nome_aux = null;

  public function __construct(FGS_usuario $u) {
    parent::__construct("cmail", self::ptw_1);

    $this->id_site = $u->atr("id_site")->valor;

    $splesk = Site_plesk_obd::inicia(new FS_cbd(), $u);

    $this->id_plesk = $splesk->atr("id_plesk")->valor;
    $this->dominio  = $splesk->atr("dominio")->valor;


    $this->pon_obxeto(new Span("leditar_dominio", "Editar"));

    $this->pon_obxeto(new Param("dominio", $this->dominio));

    $this->pon_obxeto(new CMail_lista());

    $this->pon_obxeto(Panel_fs::__bAceptar());
    $this->pon_obxeto(Panel_fs::__bCancelar());

    $this->pon_obxeto(new Div("msx_1"));
    $this->pon_obxeto(new Div("msx_2"));
    $this->pon_obxeto(new Div("msx_3"));

    $this->pon_obxeto(new Checkbox("autoresposta-enabled", true, "&nbsp;Activar / Desactivar auto-respuesta."));
    //~ $this->pon_obxeto(new Checkbox("forwarding-enabled", true, "&nbsp;Activar / Desactivar redirecci&oacute;n."));

    $this->pon_obxeto(new Span("pnome"));
    $this->pon_obxeto(Panel_fs::__text("tnome", 20, 111, "&nbsp;"));
    $this->pon_obxeto(Panel_fs::__text("tasunto", 20, 111, "&nbsp;"));
    $this->pon_obxeto(Panel_fs::__dataInput("dfin"));
    $this->pon_obxeto(new Textarea("txt", 1, 1));
    $this->pon_obxeto(new Cknovo());

    $this->pon_obxeto(new CMail_dominio($this->id_plesk));

    $this->pon_obxeto(Panel_fs::__bimage("bEngadir", Ico::bnovo, "Crea una cuenta de correo", true, "A&ntilde;adir email"));
    $this->pon_obxeto(Panel_fs::__bimage("bActualizar", Ico::breload, "Actualiza la lista de correo", true, "Actualizar"));

    $this->inicia_lista();


    $this->obxeto("leditar_dominio")->clase_css("default", "texto2_a");
    $this->obxeto("leditar_dominio")->style("default", "cursor: pointer;");
    $this->obxeto("leditar_dominio")->envia_ajax("onclick");

    $this->obxeto("txt")->style("default", "height: 5em; width: 77em;");

    $this->obxeto("dfin")->style("default", "text-align: center;");
    $this->obxeto("dfin")->pon_eventos("onchange", "tilia_validar_data(this)");

    $this->obxeto("msx_1")->clase_css("default", "texto3_erro");
    $this->obxeto("msx_2")->clase_css("default", "texto3_erro");
    $this->obxeto("msx_3")->clase_css("default", "texto3_erro");
  }

  public function id_plesk() {
    return $this->id_plesk;
  }

  public function dominio() {
    return $this->dominio;
  }

  public function cpc_miga() {
    return "correo electrónico";
  }

  public function operacion(EstadoHTTP $e) {
    $this->obxeto("msx_1")->post("");
    $this->obxeto("msx_2")->post("");
    $this->obxeto("msx_3")->post("");

    if ($this->obxeto("bEngadir")->control_evento()) return $this->operacion_bEngadir($e);

    if ($this->obxeto("bCancelar")->control_evento()) {
      $this->ptw = self::ptw_1;

      return $e;
    }

    if ($this->obxeto("leditar_dominio")->control_evento()) {
      $c = $this->obxeto("cmail_dominio");

      $c->visible = !$c->visible;

      $c->preparar_saida($e->ajax());

      return $e;
    }

    if ($this->obxeto("bAceptar")->control_evento()) return $this->operacion_bAceptar($e);

    if ($this->obxeto("bActualizar")->control_evento()) {
      $this->inicia_lista();

      return $e;
    }

    if (($e_aux = $this->obxeto("cmail_dominio")->operacion($e)) != null) return $e_aux;

    if (($e_aux = $this->obxeto("lista_mails")->operacion($e)) != null) return $e_aux;


    return null;
  }

  public function operacion_bEngadir(Epcontrol $e, $name = null) {
    $this->obxeto("tnome")->post($name);
    $this->obxeto("cknovo")->__limpar();

    $this->ptw = self::ptw_0;

    return $e;
  }

  public function operacion_bnomemail(Epcontrol $e, $name = null) {
    //~ echo $name . $e->evento()->html();

    $this->obxeto("tnome")->post($name);

    $this->email_nome_aux = $name;

    $this->ptw = self::ptw_2;

    return $e;
  }

  public function operacion_bredirecion(Epcontrol $e, $name, $mail_r = null, $ch = true) {
    //~ echo $name . $e->evento()->html();

    $this->obxeto("tnome"             )->post($mail_r);
    $this->obxeto("pnome"             )->post($name);
    //~ $this->obxeto("forwarding-enabled")->post($ch);

    $this->email_nome_aux = $name;

    $this->ptw = self::ptw_4;

    return $e;
  }

  public function operacion_bcontrasinal(Epcontrol $e, $name = null) {
    $this->obxeto("pnome")->post($name);
    $this->obxeto("cknovo")->__limpar();

    $this->ptw = self::ptw_3;

    return $e;
  }

  public function operacion_bautoresposta(Epcontrol $e, $name, $asunto, $text, $activa) {
    $this->obxeto("pnome"               )->post($name);
    $this->obxeto("tasunto"             )->post($asunto);
    $this->obxeto("txt"                 )->post($text);
    $this->obxeto("autoresposta-enabled")->post($activa);

    $this->ptw = self::ptw_5;

    return $e;
  }

  public function inicia_lista() {
    $rpc = new PleskMail($this->id_plesk);

    $a_lista = $rpc->getinfo_lista();

    $rpc->close();

    //~ echo "<pre>" . print_r($a_lista, true) . "</pre>";

    $this->err_internet = ($a_lista == null);

    $this->obxeto("lista_mails")->post_a($a_lista);


    $this->control_bEngadir();
  }

  protected function validar_0() {
    $this->validar_email($this->obxeto("tnome")->valor());
    $this->validar_k();
  }

  protected function validar_2() {
    $this->validar_email($this->obxeto("tnome")->valor());
  }

  protected function validar_3() {
    $this->validar_k();
  }

  protected function validar_4() {
    $this->validar_email($this->obxeto("tnome")->valor(), false);
  }

  protected function validar_5() {
    $erro = "";

    if (!$this->obxeto("autoresposta-enabled")->valor()) {
      if (trim($this->obxeto("tasunto")->valor()) == null) $erro .= "Debes teclear el asunto para la autorespuesta.<br />";
      if (trim($this->obxeto("txt"    )->valor()) == null) $erro .= "Debes teclear el texto para la autorespuesta.<br />";
    }

    $this->obxeto("msx_1")->post($erro);
  }

  protected function validar_email($email, $dominio_propio = true) {
    $erro = "";

    if ($dominio_propio) $email .= "@{$this->dominio}";

    if (!Valida::email($email)) $erro = Erro::email_senNome;

    $this->obxeto("msx_1")->post($erro);
  }

  protected function validar_k() {
    $erro = $this->obxeto("cknovo")->validar();

    if ($erro != null) {
      $this->obxeto("msx_2")->post($erro);

      return;
    }
    

    $this->obxeto("msx_2")->post($erro);
  }

  private function operacion_bAceptar(Epcontrol $e) {
    if ($this->ptw == self::ptw_0) return $this->operacion_bAceptar_0($e);
    if ($this->ptw == self::ptw_2) return $this->operacion_bAceptar_2($e);
    if ($this->ptw == self::ptw_3) return $this->operacion_bAceptar_3($e);
    if ($this->ptw == self::ptw_4) return $this->operacion_bAceptar_4($e);
    if ($this->ptw == self::ptw_5) return $this->operacion_bAceptar_5($e);

    return $e;
  }

  private function operacion_bAceptar_0(Epcontrol $e) {
    $this->validar_0();

    if ($this->obxeto("msx_1")->valor() != null) {
      $e->post_msx( $this->obxeto("msx_1")->valor() );
      
      return $e;
    }
    if ($this->obxeto("msx_2")->valor() != null) {
      $e->post_msx( $this->obxeto("msx_2")->valor() );
      
      return $e;
    }

    if (($r = $this->email_engadir()) != null) {
      $e->post_msx($r);
      
      return $e;
    } 

    $this->inicia_lista();

    $this->ptw = self::ptw_1;

    return $e;
  }

  private function operacion_bAceptar_2(Epcontrol $e) {
    $this->validar_2();

    if ($this->obxeto("msx_1")->valor() != null) {
      $e->post_msx( $this->obxeto("msx_1")->valor() );
      
      return $e;
    }

    $this->email_renomear();

    $this->inicia_lista();

    $this->ptw = self::ptw_1;

    return $e;
  }

  private function operacion_bAceptar_3(Epcontrol $e) {
    $this->validar_3();

    if ($this->obxeto("msx_2")->valor() != null) {
      $e->post_msx( $this->obxeto("msx_2")->valor() );
      
      return $e;
    }

    if (($r = $this->email_contrasinal()) != null) {
      $e->post_msx($r);
      
      return $e;
    } 

    $this->inicia_lista();

    $this->ptw = self::ptw_1;

    return $e;
  }

  private function operacion_bAceptar_4(Epcontrol $e) {
    $this->validar_4();

    if ($this->obxeto("msx_1")->valor() != null) {
      $e->post_msx( $this->obxeto("msx_1")->valor() );
      
      return $e;
    }

    $this->email_redirecion();

    $this->inicia_lista();

    $this->ptw = self::ptw_1;

    return $e;
  }

  private function operacion_bAceptar_5(Epcontrol $e) {
    $this->validar_5();

    if ($this->obxeto("msx_1")->valor() != null) {
      $e->post_msx( $this->obxeto("msx_1")->valor() );
      
      return $e;
    }

    $this->email_autoresposta();

    $this->inicia_lista();

    $this->ptw = self::ptw_1;

    return $e;
  }

  private function email_engadir() {
    $nome     = $this->obxeto("tnome")->valor();
    $k        = $this->obxeto("cknovo")->__k();

    $rpc = new PleskMail($this->id_plesk);

    $r = $rpc->create($nome, $k);

    $rpc->close();
    
    return $r;
  }

  private function email_renomear() {
    $renome = $this->obxeto("tnome")->valor();

    $rpc = new PleskMail($this->id_plesk);

    $rpc->rename($this->email_nome_aux, $renome);

    $rpc->close();
  }

  private function email_contrasinal() {
    $nome = $this->obxeto("pnome")->valor();
    $k    = $this->obxeto("cknovo")->__k();

    $rpc = new PleskMail($this->id_plesk);

    $r = $rpc->contrasinal($nome, $k);

    $rpc->close();
    
    return $r;
  }

  private function email_autoresposta() {
    $nome   = $this->obxeto("pnome"               )->valor();
    $asunto = $this->obxeto("tasunto"             )->valor();
    $dfin   = $this->obxeto("dfin"                )->valor();
    $txt    = $this->obxeto("txt"                 )->valor();
    $activa = $this->obxeto("autoresposta-enabled")->valor();

    //~ list($d, $m, $Y) = explode("-", $this->obxeto("dfin")->valor());

    $rpc = new PleskMail($this->id_plesk);

    $rpc->autoresposta($nome, $asunto, null, $txt, $activa);

    $rpc->close();
  }

  private function email_redirecion() {
    $email_r = $this->obxeto("tnome")->valor();

    $rpc = new PleskMail($this->id_plesk);

    $rpc->redirecion($this->email_nome_aux, $email_r, true);

    $rpc->close();
  }

  private function control_bEngadir() {
    $cbd = new FS_cbd();

    $u = Site_obd::inicia($cbd, $this->id_site)->usuario_obd($cbd);

    if (($max_mail = $u->atr("maxmail")->valor) == 0) return;

    $splus = Site_plus_obd::inicia($cbd, $this->id_site);

    if ($splus->atr("id_site")->valor != null) {    //* existe un rexistro site_plus para este site.
      if ($splus->atr("email")->valor == 0) return; //* contas de correo ilimitadas;

      $max_mail += $splus->atr("email")->valor;
    }

    $this->obxeto("bEngadir")->visible = $this->obxeto("lista_mails")->__count() < $max_mail;
  }
}

//*****************************************

final class CMail_dominio extends Componente {
  const ptw_0 = "ptw/paneis/pcontrol/mail/cmail_dominio.html";
  const ptw_1 = "ptw/paneis/pcontrol/mail/cmail_dominio.html";

  public function __construct($id_plesk) {
    parent::__construct("cmail_dominio", self::ptw_0);

    $this->visible = false;
/*
    $rpc = new PleskMail($id_plesk);

    $a_lista = $rpc->getinfo_dominio();

    $rpc->close();
*/

    $this->pon_obxeto(new Select("s_neur", array("reject"=>"Rechazar", "bounce"=>"Devolver", "forward"=>"Redirigir")));

    $this->pon_obxeto(Panel_fs::__bAceptar());
    $this->pon_obxeto(Panel_fs::__bCancelar());

    $this->obxeto("s_neur")->envia_submit("onchange");

    $this->obxeto("bCancelar")->envia_ajax("onclick");
  }

  public function operacion(EstadoHTTP $e) {
    if ($this->obxeto("bCancelar")->control_evento()) {
      $this->visible = !$this->visible;

      $this->preparar_saida($e->ajax());

      return $e;
    }

    return null;
  }
}

//*****************************************

class CMail_lista extends FS_lista implements Iterador_bd {
  private $a = null;
  private $i = -1;



  public function __construct($a = null) {
    parent::__construct("lista_mails", new CMail_ehtml());


    $this->post_a($a);
  }

  public function post_a($a = null) {
    $this->a = $a;
    $this->i = -1;
  }

  public function __i() {
    if ($this->i == 0) return "0";

    return $this->i;
  }

  public function descFila() {}

  public function next() {
    $this->i++;

    if ($this->i >= $this->__count()) {
      $this->i = -1;

      return null;
    }

    return $this->a[ $this->i ];
  }

  public function readonly($b = true) {
    $this->readonly = $b;
  }

  public function __count() {
    return count($this->a);
  }

  public function operacion(EstadoHTTP $e) {
    $evento = $e->evento();

    //~ echo $evento->html();

    if ($this->control_fslevento($evento, "bnomemail")) return $this->pai->operacion_bnomemail($e, $this->a[$evento->subnome(0)]['name']);

    if ($this->control_fslevento($evento, "bredirecion")) {
      $i = $evento->subnome(0);

      $mail   = $this->a[$i]['name'];
      $mail_r = $this->a[$i]['forwarding-address'];
      //~ $ch_ok  = $this->a[$i]['forwarding-enabled'] == "true";

      return $this->pai->operacion_bredirecion($e, $mail, $mail_r, $ch_ok);
    }

    if ($this->control_fslevento($evento, "bbloquear")) {
      $i = $evento->subnome(0);

      return $this->operacion_bbloquear($e, $this->a[$i]['name'], $this->a[$i]['mailbox-enabled']);
    }

    if ($this->control_fslevento($evento, "bcontrasinal")) return $this->pai->operacion_bcontrasinal($e, $this->a[$evento->subnome(0)]['name']);

    if ($this->control_fslevento($evento, "bautoresposta")) {
      $i = $evento->subnome(0);

      if ($this->a[$i]['autoresponder-enabled'] == "true") {
        $activa = true;
        $asunto = $this->a[$i]['autoresponder-subject'];
        $txt    = $this->a[$i]['autoresponder-text'];
      }
      else {
        $activa = false;
        $asunto = null;
        $txt    = null;
      }
      
      return $this->pai->operacion_bautoresposta($e, $this->a[$i]['name'], $asunto, $txt, $activa);
    }

    if ($this->control_fslevento($evento, "editar")) return $this->pai->operacion_bEngadir($e, $this->a[$evento->subnome(0)]['name']);

    if ($this->control_fslevento($evento, "bsupRedirecion")) return $this->operacion_bsupRedirecion($e, $evento->subnome(0));

    if ($this->control_fslevento($evento, "borrar")) return $this->operacion_borrar($e, $evento->subnome(0));

    return parent::operacion($e);
  }

  private function operacion_bbloquear(Epcontrol $e) {
    $i = $e->evento()->subnome(0);

    $rpc = new PleskMail($this->pai->id_plesk());

    $rpc->bloquear($this->a[$i]['name'], ($this->a[$i]['mailbox-enabled'] == "true"));

    $rpc->close();

    $this->pai->inicia_lista();

    return $e;
  }

  private function operacion_bsupRedirecion(Epcontrol $e, $i) {
    $rpc = new PleskMail($this->pai->id_plesk());

    $m0 = $this->a[$i]['name'];
    $mr = $this->a[$i]['forwarding-address'];

    $rpc->redirecion($m0, $mr, false);

    $rpc->close();

    $this->pai->inicia_lista();

    return $e;
  }

  private function operacion_borrar(Epcontrol $e, $i) {
    $rpc = new PleskMail($this->pai->id_plesk());

    $rpc->delete($this->a[$i]['name']);

    $rpc->close();

    $this->pai->inicia_lista();

    return $e;
  }

  protected function html_baleiro() {
    if (!$this->visible) return "";

    if ($this->pai->err_internet)
      $m = "Fallo de conexión a internet.";
    else
      $m = "No hay ning&uacute;n email.";

    return "<div class='lista_baleira txt_adv'>{$m}</div>";
  }

  protected function __iterador() {
    return $this;
  }
}

//------------------------------------------------------

final class CMail_ehtml extends FS_ehtml {
  public $nf = 0;

  public function __construct() {
    parent::__construct();

    $this->style_table = "border: 1px #aaa solid;";
    $this->align       = "left";
    $this->width       = "100%";
    $this->cellspacing = 0;
    $this->cellpadding = 2;
    $this->border      = 0;
  }

  protected function cabeceira($df = null) {
    return "<tr>
              <th>Nombre</th>
              <th></th>
              <th></th>
              <th style='text-align:right;'>Espacio</th>
              <th style='text-align:right;'>Cuota</th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
            </tr>";
  }

  protected function linha_detalle($df, $f) {
    //~ return "<tr><td colspan=10><pre>" . print_r($f, true) . "</pre></td></tr>";

    $q = ($f['mailbox-quota'] == -1)?"&nbsp;--&nbsp;":File::html_bytes($f['mailbox-quota']);

    return "<tr  >
              <td width=22%>" . self::__bnomemail($this, $f) . "</td>
              <td width=55px>" . self::__bredirecion($this, $f) . "</td>
              <td width=*>" . self::__bsupRedirecion($this, $f) . "</td>
              <td width=111px style='text-align:right;'>" . File::html_bytes($f['mailbox-usage']) . "</td>
              <td width=111px style='text-align:right;'>{$q}</td>
              <td style='min-width:3.3em;text-align:right;'>" . self::__bautoresposta($this, $f) . "</td>
              <td style='min-width:3.3em;text-align:right;'>" . self::__bbloquear($this, $f) . "</td>
              <td style='min-width:3.3em;text-align:right;'>" . self::__bcontrasinal($this, $f) . "</td>
              <td style='min-width:3.3em;text-align:right;'>" . self::__bborrar($this, $f) . "</td>
            </tr>";
  }

  private static function __sep() {
    return "<tr height=1px><td colspan=8 style='font-size: 1px; border-top: 1px solid #e6e6e6;'>&nbsp;</td></tr>";
  }

  private static function __bnomemail(CMail_ehtml $ehtml, $f) {
    if ($ehtml->xestor->readonly) return "";

    $id  = $ehtml->xestor->__i();

    $txt = "{$f['name']}<span style='color:#555555;'>@" . $ehtml->xestor->pai->dominio() . "</span>";

    $s = new Span("bnomemail", $txt);

    $s->clase_css("default" , "lista_elemento_a");

    $s->readonly = ($f['mailbox-enabled'] != "true");

    $s->title = "RENOMBRAR {$f['name']}@" . $ehtml->xestor->pai->dominio();

    $s->envia_submit("onclick");

    return $ehtml->__fslControl($s, $id)->html();
  }

  private static function __bautoresposta(CMail_ehtml $ehtml, $f) {
    if ($ehtml->xestor->readonly) return "";

    if ($f['mailbox-enabled'] != "true") return "";

    $d = $f['name'] . "@" . $ehtml->xestor->pai->dominio();

    $id  = $ehtml->xestor->__i();

    $ico = ($f['autoresponder-enabled'] == "true")?Ico::bautoresposta2:Ico::bautoresposta;

    $b = Panel_fs::__bapagado("bautoresposta", $ico, "Definir autorespuesta para {$d}", 22, "");


    return $ehtml->__fslControl($b, $id)->html();
  }

  private static function __bredirecion(CMail_ehtml $ehtml, $f) {
    $fe = ($f['forwarding-enabled'] == "true");
    $fa = ($f['forwarding-address'] != null);

    if (!$fe && !$fa) {
      if ($ehtml->xestor->readonly) return "";

      if ($f['mailbox-enabled'] != "true") return "";
    }

    $id  = $ehtml->xestor->__i(); $color = "";

    if ($fa) {
      $txt   = "<span style='font-size: 14px;'>&rarr;</span>&nbsp;{$f['forwarding-address']}";

      if (!$fe) $color = "color: #ba0000;";
    }
    else {
      $txt = "Crear&nbsp;redirecci&oacute;n";
      $color = "color: #0000ba;";
    }

    $s = new Span("bredirecion", $txt);


    $s->style("default", "cursor: pointer; {$color}");
    $s->style("readonly", $color);

    $s->readonly = $fa;

    $s->envia_submit("onclick");
    


    return $ehtml->__fslControl($s, $id)->html();
  }

  private static function __bsupRedirecion(CMail_ehtml $ehtml, $f) {
    if ($f['forwarding-address'] == null) return "";

    $id  = $ehtml->xestor->__i(); 

    $ct_0 = "{$f['name']}@" . $ehtml->xestor->pai->dominio();
    $ct_1 = $f['forwarding-address'];


    $s = new Span("bsupRedirecion", "[x]");


    $s->title = "Elminar redirección: {$ct_0} -> {$ct_1}";
    
    $s->style("default", "cursor: pointer;");


    $s->envia_submit("onclick", "¿ Deseas eliminar la redireción: {$ct_0} -> {$ct_1} ?");

    $s->pon_eventos("onmouseover", "this.style.color = '#0000ba'");
    $s->pon_eventos("onmouseout", "this.style.color = 'initial'");


    return $ehtml->__fslControl($s, $id)->html();
  }

  private static function __bbloquear(CMail_ehtml $ehtml, $f) {
    if ($ehtml->xestor->readonly) return "";

    $d = $f['name'] . "@" . $ehtml->xestor->pai->dominio();

    $id  = $ehtml->xestor->__i();

    if ($f['mailbox-enabled'] == "true") {
      $txt = "BLOQUEAR";
      $ico = Ico::bcandado;
    }
    else {
      $txt = "DESBLOQUEAR";
      $ico = Ico::bcandado2;
    }

    $b = Panel_fs::__bapagado("bbloquear", $ico, "{$txt} la cuenta {$d}", false, "");


    return $ehtml->__fslControl($b, $id)->html();
  }

  private static function __bcontrasinal(CMail_ehtml $ehtml, $f) {
    if ($ehtml->xestor->readonly) return "";

    if ($f['mailbox-enabled'] != "true") return "";

    $d = $f['name'] . "@" . $ehtml->xestor->pai->dominio();

    $id  = $ehtml->xestor->__i();

    $b = Panel_fs::__bapagado("bcontrasinal", Ico::bchave, "CAMBIAR la contrase&ntilde;a de {$d}", false, "");


    return $ehtml->__fslControl($b, $id)->html();
  }

  private static function __bborrar(CMail_ehtml $ehtml, $f) {
    if ($ehtml->xestor->readonly) return "";

    $d = $f['name'] . "@" . $ehtml->xestor->pai->dominio();

    $id  = $ehtml->xestor->__i();

    $b = Panel_fs::__bapagado("borrar", Ico::btrash, "BORRAR cuenta {$d}", false, "");

    $b->envia_submit("onclick", "&iquest; Deseas BORRAR la cuenta {$d} ?");


    return $ehtml->__fslControl($b, $id)->html();
  }

}
