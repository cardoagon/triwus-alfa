<?php

final class Editor_sprgf extends Editor_style {

  public function __construct(Paragrafo_obd $p) {
    parent::__construct($p->style_obd(), "ptw/style/editor_sprgf.html");
  }

  public function operacion(EstadoHTTP $e) {
    if (($e_aux = $this->obxeto("scolor")->operacion($e)) != null) {

      $this->pai->pai->preparar_saida($e_aux->ajax());

      return $e_aux;
    }


    return parent::operacion($e);
  }

  protected function a_props() {
    if (($a_obx = $this->obxetos()) == null) return null;

    $a = null;

    if (($borde = $a_obx['cborde']->valor()) != null) $a["border"] = $borde;
    if (($color = $a_obx['scolor']->valor()) != null) $a["background-color"] = $color;
    if (($marxe = $a_obx['cmarxe']->valor()) != null) $a["margin"] = $marxe;
    if (($padding = $a_obx['cpadding']->valor()) != null) $a["padding"] = $padding;
    if (($width = $a_obx['width']->valor()) != null) {
      $width = Erro::valida_numero($width);


      $a["max-width"] = "{$width}%";
    }
    if (($bgimx = $a_obx['bgimx']->valor()) != null) {
      $a["background-image"   ] = $bgimx;
      $a["background-repeat"  ] = "no-repeat";
      $a["background-size"    ] = "cover";
      $a["background-position"] = "center center";
    }

    return $a;
  }

  protected function post_style(Style_obd $s) {
    $a_props = $s->a_props();
    
//~ echo "<pre>" . print_r($a_props, true) . "</pre>";

    $maxwidth = ($a_props['max-width'] == null)?100:str_replace("%", "", $a_props['max-width']);

    $this->pon_obxeto(self::__number("width"   , $a_props['max-width'], 1, 100, 1, "%"));
    $this->pon_obxeto(self::__cborde("cborde"  , $a_props['border']));
    $this->pon_obxeto(self::__cmarxe("cmarxe"  , $a_props['margin']));
    $this->pon_obxeto(self::__cmarxe("cpadding", $a_props['padding']));
    $this->pon_obxeto(self::__scolor("scolor"  , $a_props['background-color']));
    $this->pon_obxeto(self::__imx   ("bgimx"   , $a_props['background-image']));

    $this->obxeto("width")->style("default", "width: 77px;");
  }
}

