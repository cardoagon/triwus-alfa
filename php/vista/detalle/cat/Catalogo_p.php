<?php

final class Catalogo_p extends Catalogo {  
  public function __construct(ICatalogo_fonte $icp, Config_obd $c, string $action, bool $hai_busca = true) {
    parent::__construct($icp, $c, $action);
  }
  
  protected function _busca(ICatalogo_fonte $icp, bool $vpa, string $action, bool $hai_busca):Cat_busca { //* IMPLEMENTA Catalogo._busca()
    return new CatP_busca($icp, $vpa, $action, $hai_busca);
  }
  
  protected function _lista(ICatalogo_fonte $icp, Config_obd $c_obd, Site_config_obd $sc_obd, string $action):Cat_lista { //* IMPLEMENTA Catalogo._lista()
    return new CatP_lista($icp, $c_obd, $sc_obd, $action);
  }
}


//*********************************************


final class CatP_lista extends Cat_lista {
  public function __construct(ICatalogo_fonte $icp, Config_obd $c_obd, Site_config_obd $sc_obd, string $action) {
    parent::__construct($icp, $c_obd, $sc_obd, $action);
  }


  protected function _ehtml():Cat_ehtml { //* IMPLEMENTA Cat_lista._ehtml()
    return new CatPlista_ehtml();
  }
}

//-----------------------------------------

final class CatPlista_ehtml extends Cat_ehtml {
  public function __construct() {
    parent::__construct();
  }
/*
  protected function iten_html($df, $f):string { //* IMPLEMENTA Cat_ehtml.iten_html()
    $s  = $this->xestor->id_site;
    $a  = $f["id_articulo"];
    $c  = $this->xestor->c_obd;
    $sc = $this->xestor->sc_obd;

    $elmt = new CatProducto_html($s, $a, true, $c, $sc);
    
    //~ if (!$elmt->visible) return "";
    
    return $elmt->html();
  }
*/
  protected function iten_html($df, $f):string {
//~ return "<div><pre>" . print_r($f, 1) . "</pre></div>";
    $s  = $this->xestor->id_site;
    $a  = $f["id_articulo"];
    $c  = $this->xestor->c_obd;
    $sc = $this->xestor->sc_obd;

    $elmt = new Elmt_ventas_FSLproducto($s, $a, true, $c, $sc);
    
    if (!$elmt->visible) return "";
    
    return $elmt->html();
  }
}


//*********************************************


final class CatP_busca extends Cat_busca {
  public function __construct(ICatalogo_fonte $icp, bool $vpa, string $action, bool $hai_busca) {
    parent::__construct($icp, $vpa, $action, $hai_busca);
  }

  protected function sql_where_txt():string { //* IMPLEMENTA Cat_busca.sql_where_txt()
    $txt = $this->obxeto("txt")->valor();

    $wh  = Valida::buscador_txt2sql( $txt, array("a.nome", "a.notas") );

    if ($wh == "") return "(1)";

    return "({$wh})";
  }

  protected function sql_where_etq(int $i):string { //* IMPLEMENTA Cat_busca.sql_where_etq()
    if (($wh = $this->chEtq_comas($i)) == null) return "(1)";
    

    switch ($i) {
      case 1: return "((a.id_categoria IN ({$wh})) or (c.id_pai IN ({$wh})) or (c.id_avo IN ({$wh})))";
      case 2: return "(a.id_marca in ({$wh}))";
    }
    
    return "(1)";
  }
}


//*****************************************************


final class CatProducto_html {
  private ?Articulo_obd $ar     = null;
  private ?Promo_obd    $p      = null;
  private ?array        $_gcc   = null;
  private ?Config_obd   $c      = null;
  private ?bool         $ive    = null;
  private ?string       $imx    = null;
  private ?string       $action = null;
  
  public function __construct(int $s, int $a, bool $vpa, Config_obd $c, Site_config_obd $sc, FS_cbd $cbd = null) {
    if ($cbd == null) $cbd = new FS_cbd();

    list($this->_gcc, $this->ar) = self::inicia_gcc($cbd, $s, $a, $vpa);
    
    $this->ive    = $sc->atr("ventas_producto_ive")->valor == 1;
    $this->c      = $c;
    $this->p      = Promo_obd::inicia_articulo($cbd, $s, $this->ar->atr("id_articulo")->valor);
    $this->imx    = $this->ar->iclogo_obd($cbd)->url(false, $cbd);
    $this->action = $this->ar->action(true);

    //~ parent::__construct( self::inicia_eobd($this->ar, $cbd) );


    //~ $this->inicia_ptw($c, $sc, false, false);
    
    //~ $this->config_urls("", false);
  }

  public function config_urls($id_js, $editor = false) {
    if ($editor        ) return;
    if (!$this->visible) return;


    $id_a = $this->ar->atr("id_articulo")->valor;

    //* configura bcomprar.
    $this->obxeto("bCarrito")->pon_eventos("onclick", "ventas2_bcarrito_post('{$id_js}', {$id_a})");

    //* configura ampliar detalle.

    $url     = $this->ar->action(true);
    $f_click = "document.location.href = '{$url}'";

    $this->obxeto("clogoarti")->url_redirect = $url;

    $this->obxeto("ldetalle" )->pon_eventos("onclick", $f_click);
    $this->obxeto("enome"    )->pon_eventos("onclick", $f_click);
  }

  public function html():string {
    //~ return "<pre>" . print_r($this->_gcc, 1) . print_r($this->ar->a_resumo(), 1) . "</pre>";
    $html = LectorPlantilla::plantilla( Refs::url($this->c->ptw_elmt("elmt_ventas")) );
    
    $html = str_replace("[clogoarti]", $this->html_clogoarti (), $html);
    $html = str_replace("[enome]"    , $this->html_nome      (), $html);
    $html = str_replace("[elmt_cc]"  , $this->html_gcc       (), $html);
    $html = str_replace("[doferta]"  , $this->html_oferta    (), $html);
    $html = str_replace("[pvp]"      , $this->html_pvp       (), $html);
    $html = str_replace("[pvpoferta]", $this->html_pvp_oferta(), $html);
    $html = str_replace("[ldetalle]" , ""                      , $html);
    
    return $html;
  }

  private function html_clogoarti():string {
    $t = $this->ar->atr("nome")->valor;
    
    return "<a href='{$this->action}'><img class='celda_imaxe_ventas' style='cursor:pointer;' title='{$t}' alt='{$t}' src='{$this->imx}'></a>";
  }

  private function html_nome():string {
    $d = new Div("enome", $this->ar->atr("nome")->valor);

    $css = "ventas_titulo";
    if ($this->ar->destacado()) $css .= " ventas_destacado";

    $d->clase_css("default", $css);
    
    $d->pon_eventos("onclick", "document.location.href = '{$this->action}'");
    

    return $d->html();
  }

  private function html_oferta():string {
    $ok_p = false; if ($this->p != null) if ($this->p->atr("tipo_dto")->valor == "p") $ok_p = true;

    $txt = ($ok_p)?$this->p->html_ventas():"OFERTA";


    $d = new Div("doferta", $txt);

    $d->clase_css("default", "ventas_oferta");

    if ($ok_p)
      $d->visible = (($this->p->atr("compra_min")->valor == 0) && ($this->p->atr("codigo")->valor == null));
    else
      $d->visible = ($this->ar->atr("desc_p")->valor > 0);


    return $d->html();
  }

  private function html_pvp():string {
    $ok_p = false; if ($this->p != null) if ($this->p->atr("tipo_dto")->valor == "p") $ok_p = true;

    $pvp = $this->ar->pvp(1, $this->ive, false, null);

    $txt = number_format($pvp, 2, ",", ".") . "&nbsp;" . Articulo_obd::$a_moeda[$this->ar->atr("moeda")->valor];

    if ($ok_p) {
      if (($this->p->atr("compra_min")->valor == 0) && ($this->p->atr("codigo")->valor == null)) $txt = "<s>{$txt}</s>";
    }
    elseif ($this->ar->atr("desc_p")->valor > 0) $txt = "<s>{$txt}</s>";


    $d = new Div("pvp", $txt);

    $d->clase_css("default", "ventas_pvp");


    return $d->html();
  }

  private function html_pvp_oferta():string {
    $ok_p = false; if ($this->p != null) if ($this->p->atr("tipo_dto")->valor == "p") $ok_p = true;

    $pvp = ($ok_p)?$this->ar->pvp(1, $this->ive, true, $this->p):$this->ar->pvp(1, $this->ive, true, null);

    $txt = number_format($pvp, 2, ",", ".") . "&nbsp;" . Articulo_obd::$a_moeda[$this->ar->atr("moeda")->valor];


    $d = new Div("pvpoferta", $txt);

    $d->clase_css("default", "ventas_pvp_oferta");

    if ($ok_p)
      $d->visible = (($this->p->atr("compra_min")->valor == 0) && ($this->p->atr("codigo")->valor == null));
    else
      $d->visible = ($this->ar->atr("desc_p")->valor > 0);


    return $d->html();
  }

  private function html_gcc():string {
    if ($this->_gcc == []) return "";

//~ print_r($this->_gcc, 1);
    
    $html_cc = "";
    foreach($this->_gcc as $n => $_x) {
      $html_cc .= ($_x["u"] >= $_x["m"]) ? "<div unid='{$_x["u"]}'>{$n}</div>" : "<div class='elmt_cc__sin-stock'>{$n}</div>";
    }

    return "<div class='elmt_cc'>
              <div class='elmt_cc__btn'></div>
              <div class='elmt_cc__lista'>{$html_cc}</div>
            </div>";
  }

  private static function inicia_gcc(FS_cbd $cbd, int $id_site, int $id_articulo, bool $vpa):array {
    //* Inicia o grupo_cc, e selecciona o primeiro artículo con stock dispoñible.


    $ar = Articulo_obd::inicia($cbd, $id_site, $id_articulo);

//~ print_r($ar->a_resumo());

    $_gcc = Articulo_cc_obd::_grupo_resumo($id_site, $ar->atr("id_grupo_cc")->valor, $vpa, $cbd); //* so buscamos artículos NON vpa.

//~ print_r($_gcc);

    if ($ar->atr("unidades")->valor >= $ar->atr("pedido_min")->valor) return [$_gcc, $ar]; 

    foreach($_gcc as $n => $_x) {
      if ($_x["a"] == $id_articulo) continue; //* xa foi evaluado e xa sabemos que non vale.
      
      if ($_x["u"] < $_x["m"]) continue;
      
      $ar = Articulo_obd::inicia($cbd, $id_site, $_x["a"]);
            
      return [$_gcc, $ar];
    }


    return [$_gcc, $ar]; //* se non atopamos ningún con unidades
  }
}

