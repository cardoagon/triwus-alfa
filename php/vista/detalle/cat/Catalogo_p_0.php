<?php

final class Catalogo_p_0 extends Componente {

  const ptw_0 = "ptw/paragrafo/elmt/elmt_ventas_post-mobil.html"; //* MODAL resposta "bComprar". Para plantillas q amosan o botón comprar na vista de resumo.
  
  public function __construct(ICatalogo_fonte $icp, Config_obd $c, string $action, bool $hai_busca = true) {
    parent::__construct("cat");


    $sc_obd = Site_config_obd::inicia(new FS_cbd(), $icp->icatF__site());

    $vpa    = ($sc_obd->atr("ventas_producto_agotados")->valor == 1);

    $this->pon_obxeto(new Hidden("_h2")); //* canal E. solicitude agregar artículo ao carriño.
    
    $this->pon_obxeto(new CatP_busca($icp, $vpa, $action, $hai_busca));
    $this->pon_obxeto(new CatP_lista($icp, $c, $sc_obd, $action));
    
//~ echo $this->obxeto("lpro")->sql_vista();

  }

  public function control_url(array $_p) {
    if (!is_array($_p) ) return; 

    
    if ( is_array($_p["_f"]) ) {
      $this->filtrar( $_p["_f"] ); //* primeiro filtrar, porque post_where(), reinicia limit_0.
    }

    $this->paxinar( $_p[1] );
    
 
//~ echo $this->obxeto("lpro")->sql_vista();
  }

  public function operacion(EstadoHTTP $e) {
    if ($this->obxeto("_h2")->control_evento()) return $this->operacion_h2($e);
    
    if (($e_aux = $this->obxeto("lpro")->operacion($e)) != null) return $e_aux;

    return null;
  }
  
  public function html():string {
    return $this->obxeto("_h2"   )->html() . 
           $this->obxeto("cbusca")->html() . 
           $this->obxeto("lpro"  )->html();
  }

  private function operacion_h2(EstadoHTTP $e) {
    $_h2 = $this->obxeto("_h2");


    $cbd = new FS_cbd();

    $a = Articulo_obd::inicia($cbd, $this->id_site, $_h2->valor());

    $u_carro = $e->usuario()->cpago()->carro();

    $i_carro = $u_carro->post($a, -1, $a->atr("pedido_min")->valor());

    $html = LectorPlantilla::plantilla( Refs::url(self::ptw_0) );

    $json = Elmt_ventas::__bcarrito_resposta_json($u_carro, $i_carro, $html, false, $e->url_limpa);

    $e->ajax()->pon_ok( true, "elmt_ventas_postResposta({$json})" );

    return $e;
  }


  private function filtrar(array $_f):void {
    //* .- realiza filtrado.
    $b = $this->obxeto("cbusca");

    if (!$b->visible) return;
    
    
    $b->operacion_txt( $_f );
    $b->operacion_f1 ( $_f );
    $b->operacion_f2 ( $_f );

    //* .- actualiza lista.
    $this->obxeto("lpro")->post_where( $b->sql_where() );
  }

  private function paxinar(?int $pax = null):void {
//~ echo "1.-pax:: $pax<br>";
    $this->obxeto("lpro")->paxinar($pax);
  }
}


//*********************************************


final class CatP_lista extends FS_lista {
  public string          $titulo;
  public string          $action;
  public int             $id_site;

  public Config_obd      $c_obd;
  public Site_config_obd $sc_obd;

  public function __construct(ICatalogo_fonte $icp, Config_obd $c_obd, Site_config_obd $sc_obd, string $action) {
    parent::__construct("lpro", new CatPlista_ehtml());

    $this->titulo  = $icp->icatF__titulo();
    $this->action  = $action;
    $this->id_site = $icp->icatF__site();

    $this->c_obd   = $c_obd;
    $this->sc_obd  = $sc_obd;
    
    
    $vpa     = ($sc_obd->atr("ventas_producto_agotados")->valor == 1);

    $this->selectfrom = $icp->icatF__selectfrom($vpa);
    $this->where      = "1";
    $this->orderby    = $icp->icatF__orderby();
    $this->limit_f    = 2;

    $this->obxeto("lista")->style("default", "margin-top: 3.3em;");

//~ echo $this->sql_vista();
  }

  protected function html_baleiro() {
    return "";
  }
}

//-----------------------------------------

final class CatPlista_ehtml extends FS_ehtml {
  public function __construct() {
    parent::__construct();

    $this->class_table = "";
    $this->style_table = "margin-top: 55px;";
    $this->width       = "100%";
    $this->cellspacing = 0;
    $this->cellpadding = 3;
    $this->border      = 0;
  }


  protected function inicia_saida() {
    return "";
  }

  protected function finaliza_saida() {
    return "";
  }

  protected function cabeceira($df = NULL) {
    return "<div><h1>{$this->xestor->titulo}</h1></div>
            <div id='cxcli3-lanuncios-0-panel'>";
  }

  protected function linha_detalle($df, $f) {
//~ return "<div><pre>" . print_r($f, 1) . "</pre></div>";
    $s  = $this->xestor->id_site;
    $a  = $f["id_articulo"];
    $c  = $this->xestor->c_obd;
    $sc = $this->xestor->sc_obd;

    $elmt = new Elmt_ventas_FSLproducto($s, $a, true, $c, $sc);
    
    if (!$elmt->visible) return "";
    
    return "<div class='ventas_div_c'>" . $elmt->html() . "</div>";
  }

  protected function totais() {
    return "</div><div class='cbuscador_cab'>" . $this->trw_paxinador( $this->xestor->action ) . "</div>";
  }
}


//*********************************************


final class CatP_busca extends Componente {
  const ptw_0 = "ptw/detalle/cat/b_0.html";

  public function __construct(ICatalogo_fonte $icp, bool $vpa, string $action, bool $hai_busca) {
    parent::__construct("cbusca", Refs::url(self::ptw_0));
    
    if (!$hai_busca) {
      $this->visible = false;
      
      return;
    }
    
    $this->pon_obxeto(new Param("action"  , $action));

    $this->pon_obxeto(new Param("pmarcas"          ));
    $this->pon_obxeto(new Param("psubcats"         ));

    $this->pon_obxeto(new Hidden("hevento"         ));


    $this->inicia($icp, $vpa);
  }

  public function operacion(EstadoHTTP $e) {
    if (!$this->visible) return null;
    
//~ echo $e->evento()->html();
    if ($this->obxeto("hevento")->control_evento()) return $this->operacion_hevento($e);


    return null;
  }

  public function sql_where() {
    if (!$this->visible) return "(1)";
    
    return $this->where_txt() . " and " . $this->where_categoria() . " and " . $this->where_marca();
  }

  public function preparar_saida(Ajax $a = null) {
    if ($this->visible) {
      $this->obxeto("hevento" )->post( null                  );
      $this->obxeto("psubcats")->post( $this->html_subcats() );
      $this->obxeto("pmarcas" )->post( $this->html_marcas()  );
    }

    parent::preparar_saida($a);
  }

  private function operacion_hevento(EstadoHTTP $e) {
    $hevento = $this->obxeto("hevento");

    switch ($hevento->valor()) {
      case "f":
        return $this->operacion_buscar($e);

      case "l":
        $this->inicia();

        return $this->operacion_buscar($e);
    }


    $e->ajax()->pon_ok();

    return $e;
  }

  private function operacion_buscar(EstadoHTTP $e) {
    //* realiza búsquedas de artículo en 'v_articulo'.
    $wh = $this->sql_where();

    return $this->pai->operacion_reset($e, $wh);
  }

  private function where_txt() {
    $txt = $this->obxeto("txt")->valor();

    $wh  = Valida::buscador_txt2sql( $txt, array("a.nome", "a.notas") );

    if ($wh == "") return "(1)";

    return "({$wh})";
  }

  private function where_categoria() {
    $_ch = $this->obxetos("chSubcat");

    if (count($_ch) == 0) return "(1)";

    $wh = null;
    foreach($_ch as $ch) {
      if (!$ch->valor()) continue;

      list($x, $id_subcat) = explode(Escritor_html::csubnome, $ch->nome());

      if ($wh != null) $wh .= ", ";

      $wh .= $id_subcat;
    }

    if ($wh == null) return "(1)";


    //~ return "((a.id_categoria IN ({$wh})) or (c.id_pai IN ({$wh})))";
    return "((a.id_categoria IN ({$wh})) or (c.id_pai IN ({$wh})) or (c.id_avo IN ({$wh})))";
  }

  private function where_marca() {
    $_ch = $this->obxetos("chMarca");

    if (count($_ch) == 0) return "(1)";

    $wh = null;
    foreach($_ch as $ch) {
      if (!$ch->valor()) continue;

      list($x, $id_marca) = explode(Escritor_html::csubnome, $ch->nome());

      if ($wh != null) $wh .= ", ";

      $wh .= "{$id_marca}";
    }

    if ($wh == null) return "(1)";


    return "(a.id_marca in ({$wh}))";
  }

  private function where_pvp() {
    return 1;
  }

  private function html_subcats() {
    if (!$this->visible) return "";
    
    $_ch = $this->obxetos("chSubcat");

    if (count($_ch) == 0) return "";

    $html = null;

    foreach($_ch as $ch) $html .= "<div>" . $ch->html() . "</div>";

    if ($html == null) return "";

    return "<div class='elmt_ventas_post_4'>CATEGORÍAS</div>" . $html;
  }

  private function html_marcas() {
    if (!$this->visible) return "";
    

    $html = null;

    $_ch = $this->obxetos("chMarca");

    if (count($_ch) == 0) return "";

    foreach($_ch as $ch) $html .= "<div>" . $ch->html() . "</div>";

    if ($html == null) return "";

    return "<div class='elmt_ventas_post_4'>MARCAS</div>" . $html;

  }

  private function inicia(ICatalogo_fonte $icp, bool $vpa) {
    $this->inicia_txt();

    $cbd = new FS_cbd();

    $this->inicia_categorias($cbd, $icp, $vpa);
    $this->inicia_marcas    ($cbd, $icp, $vpa);
  }

  private function inicia_txt() {
    $t = new Text("txt");

    $t->pon_atr("trw_novas2_busca_etq_0", "1");

    $t->placeholder = "Buscar";

    $t->pon_eventos("oninput", "prgf_inovas_clear(this)");

    $this->pon_obxeto( $t );
  }

  private function inicia_categorias(FS_cbd $cbd, ICatalogo_fonte $icp, bool $vpa) {
    //* 1.- Conta artículos por categoria

    $r = $cbd->consulta( $icp->icatF__sql_categorias($vpa) );

    $_ct = null;
    while ($_a = $r->next()) $_ct[ $_a['id_categoria'] ] = $_a['categoria'];

    if ($_ct == null) return;


    foreach($_ct as $k=>$v) $this->pon_obxeto(self::__checker("chSubcat", $v, ""), $k);
  }

  private function inicia_marcas(FS_cbd $cbd, ICatalogo_fonte $icp, bool $vpa) {
    //* 1.- Conta artículos por marca
    $r = $cbd->consulta( $icp->icatF__sql_marcas($vpa) );

    $_ct = null;
    while ($_a = $r->next()) $_ct[ $_a['id_marca'] ] = $_a['marca'];


    //* 2.- Crea Controis.
    if (!is_array($_ct)) return;

    foreach($_ct as $k=>$v) $this->pon_obxeto(self::__checker("chMarca", $v, ""), $k);
  }

  public function operacion_txt(array $_p):void {
    $t = ( isset($_p["f0"]) )? trim($_p["f0"]) : "";

    $this->obxeto("txt")->post( $t );
  }

  public function operacion_f1(array $_p):void {
    $this->reset("chSubcat");

//~ echo "111111111111111111111<br>";
    if (!isset($_p['f1'])) return;

//~ echo "222222222222222222222<br>";
    $_ch = $this->obxetos("chSubcat");

    if (count($_ch) == 0) return;


    $_p1 = explode(",", $_p['f1']);

    for ($i = 0; $i < count($_p1); $i++) {
      if (($o = $this->obxeto("chSubcat", $_p1[$i])) == null) continue;

      $o->post(true);
    }
  }

  public function operacion_f2(array $_p):void {
    $this->reset("chMarca");

    if (!isset($_p['f2'])) return;
    

    $_ch = $this->obxetos("chMarca");

    if (count($_ch) == 0) return;
    

    $_p2 = explode(",", $_p['f2']);

    for ($i = 0; $i < count($_p2); $i++) {
      if (($o = $this->obxeto("chMarca", $_p2[$i])) == null) continue;

      $o->post(true);
    }
  }

  private function reset($ch_tipo) {
    $_ch = $this->obxetos($ch_tipo);

    if (count($_ch) == 0) return true;

    foreach($_ch as $k=>$ch) $ch->post(false);


    return true;
  }

  private static function __checker($id, $etq, $ct) {
    //~ $ch = new Checkbox($id, false, "{$etq} ({$ct})");
    $ch = new Checkbox($id, false, $etq);

    return $ch;
  }
}

