<?php

final class Prgf_iventas2b extends Paragrafo
                        implements IPrgf_iventas {
 
  public int $id_site;
                         
  private Paxina_ventas2_obd $p;
                            
  public function __construct(Paxina_ventas2_obd $p) {
    parent::__construct($p->prgf_indice());

    $this->alg_html_elmts = 10;
    $this->usa_bpos       = false;
    $this->excluir        = 1;

    $this->id_site        = $p->atr("id_site")->valor; //* compatibilidade con Editor_prgf_ventas2.
    
    //* config ICatalogo_fonte.
    $this->p                  = $p;
    $this->p->id_categoria    = $this->iventas_categoria;
    
    $this->p->icatF__orderby = $this->orderby();
  }

  public function inicia_ptw(Config_obd $c) {
    $action    = $this->p->atr("action")->valor . "?ctlg=" . $this->p->atr("id_idioma")->valor;
    $hai_busca = $this->iventas_buscador == 1;
    

    $this->pon_obxeto( new Catalogo_p( $this->p, $c, $action, $hai_busca) );
  }

  public function operacion(EstadoHTTP $e) {
    if ( ($e_aux = parent::operacion($e)                ) != null ) return $e_aux;

    if ( ($e_aux = $this->obxeto("cat")->operacion($e)) != null ) return $e_aux;

    return $this->operacion_url($e);
  }

  public function operacion_editar(Epaxina_edit $e) { 
    $e->obxeto("detalle_btnr_oeste")->pon_editor($e, new Editor_prgf_ventas2($this));

    return $e;
  }

  public function html_ventas2b() {
    if (($catp = $this->obxeto("cat")) == null) return "";
    
    return $catp->html();
  }

  protected function pon_btnr() {
    $this->pon_obxeto(new Btnr_prgf(false));
  }

  private function operacion_url(EstadoHTTP $e) {
    $ev = $e->evento();

//~ echo $ev->html();

    if ($ev->tipo()  != "op-reservada") return null;

    if ($ev->nome(0) != "catalogo"    ) return null;

    $_param = $ev->nome(1);

    if ( !is_array($_param) ) {
      $e->redirect("index.php");

      return $e;
    }


    $this->obxeto("cat")->control_url($_param);
 
    
    return $e;
  }

  private function orderby() {
    if ($this->orderby == 1) return "momento desc";

    if ($this->orderby == 2) return "momento asc";

    if ($this->orderby == 3) return "nome asc";

    return "rand()"; //* opción ($this->orderby == 9) predeterminada.
  }
}
