<?php

final class Prgf_inovas2 extends Paragrafo {
  public $id_site           = null;
  public $id_paxina         = null;
  public $id_opcion         = null;
  public $id_idioma         = null;
  public $config            = null;
                            
  //~ public $crs               = null;
                            
  //* filtro editor.        
  public $f_txt             = null;
  public $f_dini            = null;
  public $f_dfin            = null;
                            
  private $u_permisos       = 0;
                            
  private $pax_i            = 0;
  private $pax_t            = 30;
  
  private $inovas_categoria = 0;
  private $inovas_buscador  = 0;
                            
  private $where_aux        = null; //* Where auxiliar.
  private $where_pub        = null; //* Where data publicación.
                            
  private $sql_vista        = null; 

  public function __construct(Paxina_novas2_obd $p, $editor) {
    parent::__construct($p->prgf_indice(), "prgf", false);

    $this->id_site        = $p->atr("id_site"  )->valor;
    $this->id_paxina      = $p->atr("id_paxina")->valor;
    $this->id_opcion      = $p->atr("id_opcion")->valor;
    $this->id_idioma      = $p->atr("id_idioma")->valor;

    $this->alg_html_elmts = 9;
    $this->usa_bpos       = false;
    $this->excluir        = 1;

    //~ $this->crs            = self::inicia_crs( $this->crs_obd );

    $this->pon_obxeto( Panel_fs::__bmais  ("bEngadir", "Añadir entrada") );
    $this->pon_obxeto( Panel_fs::__beditar("bFiltro" , "Aplicar filtro") );

    $this->pon_obxeto(new Hidden("_hb" ) ); //* canal E/S. solicitude de artículos.
    $this->pon_obxeto(new Hidden("_h0" ) ); //* canal E. lista id_articulos.
    $this->pon_obxeto(new Div   ("_h1" ) ); //* canal S. resposta html.

    $this->pon_obxeto(new Hidden("_h10") ); //* canal E. solicitude edición dunha nova.


    $this->pon_obxeto( new Novas2_buscador($this->id_site, $this->id_opcion, $this->id_idioma, $this->inovas_etqs) );


    $this->obxeto("bEngadir")->envia_ajax("onclick");
    $this->obxeto("bFiltro" )->envia_ajax("onclick");

    $this->obxeto("_h1")->style("default", "display: none;");
  }

  public function config_usuario(IEfspax_xestor $e = null) {
    $this->inicia_ptw($e->config());
    
    $this->u_permisos = $e->usuario()->a_permisos();

    $this->where_pub($e->edicion());

    parent::config_usuario($e);
  }

  public function declara_js() {
    return array(Refs::url("js/novas2.js"));
  }

  public function inicia_ptw(Config_obd $c) {
    $this->config = $c;

    //~ $this->crs->config_rs($c);
  }

  public function where_aux($where = -1) {
    //* GETTER/SETTER

    if ($where != -1) $this->where_aux = $where;

    if ($this->where_aux != null) return $this->where_aux;


    if (($where2 = $this->obxeto("ccatalogo")->sql_where()) == "(1)") return null;


    return $where2;
  }

  public function operacion(EstadoHTTP $e) {
//~ echo $e->evento()->html();

    if (($e_aux = $this->operacion_url_filtrado($e))         != null) return $e_aux;

    if ($this->obxeto("_hb" )->control_evento()) return $this->operacion_hb ($e);
    if ($this->obxeto("_h0" )->control_evento()) return $this->operacion_h0 ($e);
    if ($this->obxeto("_h10")->control_evento()) return $this->operacion_h10($e);

    if ($this->obxeto("bEngadir")->control_evento()) return $this->operacion_editar_nova($e);
    if ($this->obxeto("bFiltro" )->control_evento()) return $this->operacion_filtrar    ($e);


    return parent::operacion($e);
  }

  public function operacion_editar(Epaxina_edit $e) {
    $e->obxeto("detalle_btnr_oeste")->pon_editor($e, new Editor_prgf_novas2($this));

    return $e;
  }

  public function operacion_editar_nova(Epaxina_edit $e, $id_paxina = null) {
    //~ $ptw     = $this->config->ptw_elmt("elmt_novas_1");
    $url_imx = $e->url_arquivos();

    $editor  = new Editor_novas2($this, $url_imx, $id_paxina);

    $e->obxeto("detalle_btnr_oeste")->pon_editor($e, $editor);

    return $e;
  }

  public function operacion_filtrar(Epaxina_edit $e) {
    $editor  = new Editor_novas2_filtro($this);

    $e->obxeto("detalle_btnr_oeste")->pon_editor($e, $editor);

    return $e;
  }

  public function operacion_duplicar_nova(Epaxina_edit $e, $id_paxina) {
    $cbd = new FS_cbd();

    $cbd->transaccion();

    if (($id_paxina_d = $this->operacion_duplicar_nova_2($cbd, $id_paxina)) > 0) {
      $cbd->commit();

      $this->obxeto("_h1")->post( $this->html_elmt($cbd, $id_paxina_d, false) );
      
      $e->ajax()->pon( $this->obxeto("_h1") );
      $e->ajax()->pon_ok(true, "novas2_resposta_duplicar({$id_paxina}, {$id_paxina_d})");
      
      return $e;
    }
    
    
    $cbd->rollback();

    $e->post_msx("Sucedió un error al actualizar la BD.");


    return $e;
  }

  public function operacion_suprime_nova(Epaxina_edit $e, $id_paxina) {
    $cbd = new FS_cbd();

    $cbd->transaccion();

    $p = Paxina_obd::inicia($cbd, $id_paxina, "novas2");


    if ($p->delete($cbd)) {
      $cbd->commit();
      
      //~ $this->preparar_saida( $e->ajax() );
      $e->ajax()->pon_ok(true, "novas2_resposta_suprimir({$id_paxina})");
      
      return $e;
    }
    

    $cbd->rollback();

    $e->post_msx("Sucedió un error al actualizar la BD.");

    return $e;
  }

  public function operacion_reset(IEfspax_xestor $e, $where = null) {
    //* carga articulos segundo a seleción do buscador de triwus.
    $this->obxeto("_h1")->post(null);

    $this->post_hb($where, true);

    $e->ajax()->pon($this->obxeto("_hb"), true, "novas2_b0_resposta(" . $this->json_o(true) . ")");


    return $e;
  }

  public function html_novas2() {
    //~ return "<h1>prgf_inovas2</h1>" . $this->obxeto("_hb")->valor();

    return
$this->obxeto("_hb")->html() .
$this->obxeto("_h0")->html() .
$this->obxeto("_h1")->html() .
$this->obxeto("_h10")->html() . "
<input type='hidden' id='trw-catalogo-id' value='" . $this->nome_completo() . "' />
" . $this->html_buscador() . "
<div id='cxcli3-lanuncios-0'>
  " . $this->html_editor() . "
  <div id='cxcli3-lanuncios-0-panel' class='" . Prgf_html::__elmts_1_class($this) . "'></div>
</div>

<script>
  novas2_ini(" . $this->json_o(true) . ");
</script>";
  }

  public function preparar_saida(Ajax $a = null) {
    parent::preparar_saida($a);

    if ($a == null) return;

    $a->pon_ok( true, "novas2_ini(" . $this->json_o(true) . ")" );
  }

  protected function pon_btnr() {
    $this->pon_obxeto(new Btnr_prgf(false));
  }

  private function operacion_hb(EstadoHTTP $e) {
    //* inicia artículos na páxina de resumo.

    $reinicia = ($this->obxeto("_hb")->valor() == 1);

    $this->post_hb($this->where_aux(), $reinicia);


    $e->ajax()->pon($this->obxeto("_hb"));


    return $e;
  }

  private function operacion_h0(EstadoHTTP $e) {
    $_h0 = $this->obxeto("_h0");
    $_h1 = $this->obxeto("_h1");

    $_a = explode(",", $_h0->valor());

    $cbd = new FS_cbd();

    $html = ""; for ($i = 0; $i < count($_a); $i++) {
      $html .= $this->html_elmt($cbd, $_a[$i]);
    }

//~ echo "<pre>" . print_r($_a, true) . "</pre>";

    $_h1->post($html);

    $e->ajax()->pon( $_h1 );

    return $e;
  }

  private function operacion_h10(EstadoHTTP $e) {
   list($codigo, $id_paxina) = json_decode( $this->obxeto("_h10")->valor() );

//~ echo "$codigo, $id_paxina<br>";

    switch ($codigo) {
      case "edit1":
        return $this->operacion_editar_nova($e, $id_paxina);

      case "edit2":
        return Efspax_xestor::operacion_redirect($e, $id_paxina, 1);

      case "edit3":
        return $this->operacion_duplicar_nova($e, $id_paxina);

      case "suprime":
        return $this->operacion_suprime_nova($e, $id_paxina);
    }

    if (strpos($codigo, "move") === 0) return $this->operacion_mover_nova($e, $codigo, $id_paxina);

    return $e;
  }

  private function operacion_duplicar_nova_2(FS_cbd $cbd, $id_paxina):int {
    $p = Paxina_obd::inicia($cbd, $id_paxina, "novas2");

    $b = $p->duplicar_paxina_detalle($cbd, $this->id_idioma);
    
    if ($b) return $p->atr("id_paxina")->valor;
    
    return -1;
  }

  private function operacion_mover_nova(Epaxina_edit $e, $codigo, $id_paxina_0) {
    list($x, $id_paxina_1) = explode("::", $codigo);

    $cbd = new FS_cbd();

    $cbd->transaccion();

    $b = Paxina_novas2_obd::update_mover($cbd, $this->id_opcion, $this->id_idioma, $id_paxina_0, $id_paxina_1);

    if ($b) {
      $cbd->commit();
      
      //~ $this->preparar_saida( $e->ajax() );
      $e->ajax()->pon_ok(true, "novas2_resposta_intercambiar({$id_paxina_0}, {$id_paxina_1})");
      
      return $e;
    }
    

    $cbd->rollback();

    
    $e->post_msx("Sucedió un error al actualizar la BD.");
    

    return $e;
  }

  private function operacion_url_filtrado(EstadoHTTP $e) {
    //* 1.- control de evento de filtrado.
    if ($e->evento()->tipo() != "op-reservada") return null;

//~ echo $e->evento()->html();

    if ($e->evento()->nome(0) != "filtrado") return null;

    if (!is_array($e->evento()->nome(1))) return null;

    $_p = $e->evento()->nome(1);

    if ($this->iventas_buscador == 1) {
      //* executa filtrado con buscador trw (buscador predeterminado).
      $this->obxeto("ccatalogo")->operacion_txt($_p);

      $this->obxeto("ccatalogo")->operacion_etq($_p, 1);
      $this->obxeto("ccatalogo")->operacion_etq($_p, 2);
      $this->obxeto("ccatalogo")->operacion_etq($_p, 3);

      return $e;
    }

    return $e;
  }

  public function html_elmt(FS_cbd $cbd, $id_paxina, bool $capa_lista = true) {
    $p = Paxina_obd::inicia($cbd, $id_paxina, "novas2");

    if ( !Erro::__parse_permisos( Opcion_ms_obd::explode_grupo($p->atr("grupo_aux")->valor), $this->u_permisos) ) return "";


    return INovas2_elmt::html($p, $this, $capa_lista);
  }

  private function html_buscador() {
    if ($this->iventas_buscador != 1) return "";

    $html = LectorPlantilla::plantilla( Refs::url("ptw/paragrafo/prgf_cbuscador.html") );

    $html = str_replace("[ccatalogo]", $this->obxeto("ccatalogo")->html(), $html);

    return $html;
  }

  private function html_editor():string {
    $editor = $this->pai->__editor;
    
    $this->obxeto("bEngadir")->readonly = !$editor;
    $this->obxeto("bFiltro" )->readonly = !$editor;
    
    if ( !$editor ) return "";
    
    
    return 
"<div>
" . $this->obxeto("bEngadir")->html() . "
" . $this->obxeto("bFiltro" )->html() . "
</div>";
  }

  private function json_o($limpar = false) {
    $id_js     = $this->nome_completo();
    $limpar2   = ($limpar)?"1":"0";
    $b_visible = ($this->inovas_buscador == 1)?"1":"0";

    $_o = array("s"      => $this->id_site,
                "k"      => $this->id_paxina,
                "busca"  => array("_p"        => null,
                                  "visible"   => $b_visible,
                                  "categoria" => $this->inovas_categoria,
                                  "a"         => $this->inovas_buscador == 2
                                 ),
                "id_js"  => "{$id_js}",
                "limpar" => "{$limpar2}",
                "pax_t"  => "{$this->pax_t}",
                "a"      => "{$this->action}"
               );

    if ($this->sql_vista != null) $_o["sql_vista"] = $this->sql_vista;

    return json_encode($_o);
  }

  private function post_hb($where = null, $reinicia = true) {
    //* 1.- Calcula sql.
    $w = "(a.id_opcion = {$this->id_opcion} and c.id_idioma = '{$this->id_idioma}' and a.id_prgf_indice = 0 and {$this->where_pub})";

    if ($where != null) $w .= " and ({$where})";

    $sql = "select b.id_paxina
              from paxina a inner join paxina_novas b on (a.id_paxina = b.id_paxina)
                            inner join v_ml_paxina  c on (a.id_paxina = c.id_paxina)
             where {$w}" . $this->orderby() . $this->limit($reinicia);


    //* 2.- Lee id_paxina[] na BD.

    $cbd = new FS_cbd();

    $r = $cbd->consulta($sql);

    $_a = array(); while ($_r = $r->next()) $_a[] = $_r['id_paxina'];


//~ echo "<pre>" . print_r($_a, true) . "</pre>";


    //* 3.- Asigna id_paxina[] ao buffer _hb.

    $this->obxeto("_hb")->post( json_encode($_a) );
  }

  private function orderby() {
    return " order by b.pos desc";
    //~ return " order by a.creado desc";
  }

  private function limit($reinicia = true) {
//~ echo "{$this->pax_i}::{$this->pax_t}::{$reinicia}/n";

    //* 1.- actualiza contadores.
    $this->pax_i = ($reinicia)?"0":$this->pax_i + $this->pax_t;

    //* 2.- crea a instrucción sql limit, ($l).
    if (($i = $this->pax_i) == 0) $i = "0";

    $l = " limit {$i}, {$this->pax_t}";

    //* 3.- devolve a instrucción sql limit.
    return $l;
  }

  private function where_pub(bool $editor):void {
    if ($editor) {
      $this->where_pub = "(1)";
      
      return;
    }
    
    //~ $this->where_pub = "(1)";
    $this->where_pub = "((a.publicar is null) or (now() > a.publicar))";
  }

}

//******************************

final class Novas2_buscador extends Componente {
  const ptw_0 = "ptw/paragrafo/prgf_inovas_busca.html";

  public $id_site;
  public $id_opcion;
  public $id_idioma;
  public $inovas_etqs;


  public function __construct($id_site, $id_opcion, $id_idioma, $inovas_etqs) {
    parent::__construct("ccatalogo", Refs::url(self::ptw_0));

    $this->id_site      = $id_site;
    $this->id_opcion    = $id_opcion;
    $this->id_idioma    = $id_idioma;
    $this->inovas_etqs  = unserialize($inovas_etqs);


    $this->pon_obxeto(new Param("petq"), 1);
    $this->pon_obxeto(new Param("petq"), 2);
    $this->pon_obxeto(new Param("petq"), 3);

    $this->pon_obxeto(new Hidden("hevento"));


    $this->inicia_buscador();
  }

  public function operacion(EstadoHTTP $e) {
//~ echo $e->evento()->html();

    if ($this->obxeto("hevento")->control_evento()) return $this->operacion_hevento($e);


    return null;
  }

  public function operacion_f(EstadoHTTP $e, $_p) {
    return $this->operacion_buscar($e);
  }

  public function sql_where() {
    return $this->where_txt() . " and " . $this->where_etq(1) . " and " . $this->where_etq(2) . " and " . $this->where_etq(3);
  }

  public function preparar_saida(Ajax $a = null) {
    $this->obxeto("hevento"   )->post( null               );

    $this->obxeto("petq"   , 1)->post( $this->html_etq(1) );
    $this->obxeto("petq"   , 2)->post( $this->html_etq(2) );
    $this->obxeto("petq"   , 3)->post( $this->html_etq(3) );

    parent::preparar_saida($a);
  }

  private function operacion_hevento(EstadoHTTP $e) {
    $hevento = $this->obxeto("hevento");

    switch ($hevento->valor()) {
      case "f":
        return $this->operacion_buscar($e);

      case "l":
        $this->inicia_buscador();

        return $this->operacion_buscar($e);
    }


    $e->ajax()->pon_ok();

    return $e;
  }

  private function operacion_buscar(EstadoHTTP $e) {
    //* realiza búsquedas de artículo en 'v_articulo'.
    $wh = $this->sql_where();

    return $this->pai->operacion_reset($e, $wh);
  }

  private function where_txt() {
    $txt = $this->obxeto("txt")->valor();

    $wh  = Valida::buscador_txt2sql( $txt, array("a.nome", "a.titulo", "a.descricion", "b.etq_1", "b.etq_2", "b.etq_3") );

    if ($wh == "") return "(1)";

    return "({$wh})";
  }

  private function where_etq($i) {
    $_ch = $this->obxetos("chEtq_{$i}");

    if (count($_ch) == 0) return "(1)";

    $wh = null;
    foreach($_ch as $ch) {
      if (!$ch->valor()) continue;

      list($x, $etq) = explode(Escritor_html::csubnome, $ch->nome());

      if ($wh != null) $wh .= " or ";

      $wh .= "(etq_{$i} like '%{$etq}%')";
    }

    if ($wh == null) return "(1)";


    return "({$wh})";
  }

  private function html_etq($i) {
    if ($this->inovas_etqs["b"][$i] != 1) return "";

    $_ch = $this->obxetos("chEtq_{$i}");

    if (count($_ch) == 0) return "";

    $html = null;

    foreach($_ch as $ch) $html .= "<div>" . $ch->html() . "</div>";

    if ($html == null) return "";

    return "<div class='elmt_ventas_post_4'>{$this->inovas_etqs["t"][$i]}</div>" . $html;
  }

  private function inicia_buscador() {
    $this->inicia_txt();

    $ok1 = false; $ok2 = false; $ok3 = false;

    $cbd = new FS_cbd();

    $ok1 = $this->inicia_etiquetas($cbd, 1);
    $ok2 = $this->inicia_etiquetas($cbd, 2);
    $ok3 = $this->inicia_etiquetas($cbd, 3);

    $this->visible = ( $ok1 || $ok2 || $ok3 );
  }

  private function inicia_txt() {
    $t = new Text("txt");

    $t->pon_atr("trw_novas2_busca_etq_0", "1");

    $t->pon_atr("trw_prgf_ibusca_t", "1");

    $t->pon_eventos("oninput", "prgf_inovas_clear(this)");

    $this->pon_obxeto( $t );
  }

  private function inicia_etiquetas(FS_cbd $cbd, $i) {

    //* 1.- Selecionamos as diferentes etiquetas para o nivel de etiquetado $i.

    $sql = "select distinct b.etq_{$i}
            from paxina a inner join paxina_novas b on (a.id_paxina = b.id_paxina)
                          inner join v_ml_paxina  c on (a.id_paxina = c.id_paxina)
           where (a.id_opcion = {$this->id_opcion} and c.id_idioma = '{$this->id_idioma}' and a.id_prgf_indice = 0)";


    $r = $cbd->consulta($sql);

    $_etq = null;
    while ($_r = $r->next()) {
      if ($_r["etq_{$i}"] == null) $_r["etq_{$i}"] = "";
      
      $_etq2 = preg_split("/[,;:]+/", trim($_r["etq_{$i}"]) );

      if (!is_array($_etq2)) continue;

      foreach ($_etq2 as $etq2) {
        if (trim($etq2) == "") continue;

        $_etq[$etq2] = ( isset($_etq[$etq2]) )?$_etq[$etq2] + 1:1;
      }
    }


    if ($_etq == null) return false;


    //* 2.- Crea Controis (checkers).
    ksort($_etq);
    foreach($_etq as $etq => $ct) {
      $this->pon_obxeto(self::__checker("chEtq_{$i}", $etq, $ct), $etq);
    }

    return true;
  }

  public function operacion_txt($_p) {
    $txt = ( isset($_p["f0"]) )?$_p["f0"]:null;
    
    $this->obxeto("txt")->post( $txt );

    return true;
  }

  public function operacion_etq($_p, $i) {
    $this->reset("chEtq_{$i}");

    if ( !isset($_p["f{$i}"]) ) return false;

    $_ch = $this->obxetos("chEtq_{$i}");

    if (count($_ch) == 0) return false;

    $ok = false;

    $_pi = explode(",", $_p["f{$i}"]);

    for ($i2 = 0; $i2 < count($_pi); $i2++) {
      if (($o = $this->obxeto("chEtq_{$i}", $_pi[$i2])) == null) continue;

      $ok = true;

      $o->post(true);
    }

    return $ok;
  }

  private function reset($ch_tipo) {
    $_ch = $this->obxetos($ch_tipo);

    if (count($_ch) == 0) return true;

    foreach($_ch as $k=>$ch) $ch->post(false);


    return true;
  }

  private static function __checker($id, $etq, $ct) {
    //~ $ch = new Checkbox($id, false, "{$etq} ({$ct})");
    $ch = new Checkbox($id, false, $etq);

    $ch->pon_atr("trw_prgf_ibusca_ch", "1");

    return $ch;
  }
}
