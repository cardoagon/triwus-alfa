<?php

class Editor_prgf_novas2 extends Editor_prgf {

  public function __construct(Prgf_inovas2 $p) {
    parent::__construct($p);

    $this->ptw = "ptw/paragrafo/editor_prgf_novas2.html";

    $this->obxeto("sColumnas")->sup_opcion(5);
    $this->obxeto("sColumnas")->sup_opcion(6);


    $this->pon_obxeto(self::_sbuscador($p));
    $this->pon_obxeto(self::_timx_proporcion($p));

    $this->pon_etiquetado($p);

    $this->pon_contidosRel($p);
  }

  public function operacion(EstadoHTTP $e) {
    //~ if (($e_aux = $this->obxeto("crs_editor")->operacion($e)) != null) return $e_aux;

    return parent::operacion($e);
  }

  public function operacion_aceptar(Efs_admin $e) {
    //~ if (($e_aux = $this->obxeto("crs_editor")->operacion_aceptar($e)) != null) return $e_aux;

    $this->prgf->iventas_buscador      = $this->obxeto("sbuscador"      )->valor();
    $this->prgf->inovas_imx_proporcion = $this->obxeto("timx_proporcion")->valor();
    $this->prgf->inovas_etqs           = serialize( $this->_etiquetado() );
    $this->prgf->inovas_rel_b          = ($this->obxeto("chcontidos")->valor())? 1 : null;
    $this->prgf->inovas_rel_t          = trim($this->obxeto("tcontidos")->valor());
    $this->prgf->inovas_rel_s          = $this->obxeto("scontidos")->valor();

    $this->prgf->preparar_saida($e->ajax());


    return parent::operacion_aceptar($e);
  }

  public function operacion_cancelar(Epaxina_edit $e) {
    //~ if (($e_aux = $this->obxeto("crs_editor")->operacion_cancelar($e)) != null) return $e_aux;

    return Editor_btnr_oeste::operacion_cancelar($e);
  }

  private function _etiquetado() {
    return array("b" => array(1 => $this->obxeto("etq_b", 1)->valor()?1:0,
                              2 => $this->obxeto("etq_b", 2)->valor()?1:0,
                              3 => $this->obxeto("etq_b", 3)->valor()?1:0
                             ),
                 "t" => array(1 => $this->obxeto("etq_t", 1)->valor(),
                              2 => $this->obxeto("etq_t", 2)->valor(),
                              3 => $this->obxeto("etq_t", 3)->valor()
                             )                 );
  }

  private function pon_etiquetado(Prgf_inovas2 $p) {
    if (($_etq = $p->inovas_etqs) == null) {
      $_etq = array("b" => array(1 => false, 2 => false, 3 => false),
                    "t" => array(1 => "Etiquetado 1" , 2 => "Etiquetado 2" , 3 => "Etiquetado 3" )
                   );
    }
    else
      $_etq = unserialize($_etq);


    for ($i = 1; $i < 4; $i++) {
      $this->pon_obxeto(new Checkbox("etq_b", $_etq["b"][$i], "Nivel {$i}"), $i);

      $t = new Text("etq_t");

      $t->post($_etq["t"][$i]);

      $this->pon_obxeto($t, $i);
    }

  }

  private function pon_contidosRel(Prgf_inovas2 $p) {
    //* checker.
    $this->pon_obxeto(new Checkbox("chcontidos", $p->inovas_rel_b == 1, "Mostrar"));

    //* texto.
    $t = new Text("tcontidos");

    $t->post($p->inovas_rel_t);

    $this->pon_obxeto($t);
    
    //* selector.
    $s = new Select("scontidos", [1=>"Posición"]);
    //~ $s = new Select("scontidos", [1=>"Posición", 2=>"Etiquetado"]);
    
    $s->post($p->inovas_rel_s);
    
    $this->pon_obxeto($s);
  }

  private static function _timx_proporcion(Prgf_inovas2 $p) {
    $t = Panel_fs::__text("timx_proporcion", 5, 5);

    $t->post($p->inovas_imx_proporcion);

    $t->style("default" , "text-align: right;");
    $t->style("readonly", "text-align: right;");


    return $t;
  }

  private static function _sbuscador(Prgf_inovas2 $p) {
    $_o = array("1" => "Usar buscador predeterminado",
                //~ "2", "Usar buscador a medida",
                "9" => "Sin buscador"
               );

    $s = new Select("sbuscador", $_o);

    $s->post($p->iventas_buscador);

    $s->envia_AJAX("onchange");


    return $s;
  }
}

//**********************************************

class Editor_novas2 extends Editor_prgf {
  const ptw = "ptw/paragrafo/editor_novas2.html";

  private $pax            = null;
  private $url_arquivos   = null;
  //~ private $url_upload     = null;
  private $imx_proporcion = null;


  public function __construct(Prgf_inovas2 $p, $url_arquivos, $id_paxina = null) {
    parent::__construct($p);

    $this->ptw            = self::ptw;
    $this->titulo         = "Editor de noticias";
    $this->url_arquivos   = $url_arquivos;

    $this->pax            = Paxina_obd::inicia(new FS_cbd(), $id_paxina, "novas2");
    $this->imx_proporcion = ($p->inovas_imx_proporcion > 0)?round(1 / $p->inovas_imx_proporcion, 3):"null";

    $this->inicia();
  }

  public function operacion(EstadoHTTP $e) {
    if ($this->obxeto("f_imaxe")->control_evento()) {
      $f = $this->obxeto("f_imaxe")->post_f();
      
      $this->obxeto("imaxe")->post( $f );

      $this->preparar_saida($e->ajax());

      return $e;
    }


    return parent::operacion($e);
  }

  public function operacion_aceptar(Efs_admin $e) {
    //* Valida nome
    if (($nome = trim($this->obxeto("tnome" )->valor())) == "") {
      $e->post_msx("ERROR, debes teclear el nombre de la noticia.");
      
      return $this->operacion_cancelar($e); //* non marcamos no editor a opción de actualizar.
    }
    
    
    //* Actualiza paxina_obd
    if ($this->pax->atr("id_paxina")->valor == null) {
      $this->pax->atr("id_opcion")->valor = $e->id_oms();
    }

    $this->pax->atr("nome"      )->valor = $nome;
    $this->pax->atr("titulo"    )->valor = $nome;
    $this->pax->atr("descricion")->valor = $this->obxeto("tnotas")->valor();


     // fecha/hora do evento/noticia
    if ($this->obxeto("tdata")->valor() == null)
      $this->pax->atr("data")->valor = null;
    elseif ($this->obxeto("thora")->valor != null) 
      $this->pax->atr("data")->valor = $this->obxeto("tdata")->valor() . " " . $this->obxeto("thora" )->valor() . ":00";
    else
      $this->pax->atr("data")->valor = $this->obxeto("tdata")->valor() . " " . "00:00:00";
    
    
    
    // fecha/hora para programar publicación
    if ($this->obxeto("tdatapub")->valor() == null)
      $this->pax->atr("publicar")->valor = null;
    elseif ($this->obxeto("thorapub")->valor != null) 
      $this->pax->atr("publicar")->valor = $this->obxeto("tdatapub")->valor() . " " . $this->obxeto("thorapub" )->valor() . ":00";
    else
      $this->pax->atr("publicar")->valor = $this->obxeto("tdatapub")->valor() . " " . "00:00:00";     

    $this->pax->atr("etq_1")->valor = $this->obxeto("tetq_1")->valor();
    $this->pax->atr("etq_2")->valor = $this->obxeto("tetq_2")->valor();
    $this->pax->atr("etq_3")->valor = $this->obxeto("tetq_3")->valor();

    //* Actualiza na BD.
    $cbd = new FS_cbd();

    $cbd->transaccion();

    if ( $this->update($cbd, $e->id_idioma()) ) {
      $cbd->commit();

      $id_paxina = $this->pax->atr("id_paxina")->valor;
      
      $this->prgf->obxeto("_h1")->post( $this->prgf->html_elmt($cbd, $id_paxina, false) );
      
      $e->ajax()->pon( $this->prgf->obxeto("_h1") );
      $e->ajax()->pon_ok(true, "novas2_resposta_editar({$id_paxina})");
    }
    else {
      $cbd->rollback();

      $e->post_msx("Sucedió un error al actualizar la BD.");
    }
    
    return $this->operacion_cancelar($e); //* non marcamos no editor a opción de actualizar.    
  }

  public function operacion_cancelar(Epaxina_edit $e) {
    return Editor_btnr_oeste::operacion_cancelar($e);
  }

  public function preparar_saida(Ajax $a = null) {
    $this->obxeto("hcrop")->post($this->imx_proporcion);

    parent::preparar_saida($a);

    if ($a == null) return;

    $a->pon_ok(true, "novas2_editar_1_click_resposta()");
  }

  public function declara_baceptar() {
    $b = Panel_fs::__baceptar();

    $b->pon_eventos("onclick", "elmt_novas_bAceptar_click(this, 'findex;detalle_btnr_oeste;editor')");

    return $b;
  }


  private function inicia() {
    //* inicia controis.

//~ echo "<pre>" . print_r($this->pax->a_resumo(), 1) . "</pre>";
//~ echo $this->pax->atr("data"      )->fvalor("Y/m/d"). "<br>";
//~ echo $this->pax->atr("data"      )->fvalor("H:i"  ). "<br>";


    $this->pon_obxeto(new Hidden("hcrop"));

    $this->pon_obxeto($this->__imx( $this->pax ));

    $this->pon_obxeto($this->__fimaxe( Efs::url_tmp($this->url_arquivos) ));


    $this->pon_obxeto(self::__lcambiar());

    $this->pon_obxeto(new Text("tnome"));
    $this->pon_obxeto(new Textarea("tnotas", 2, 22));

    $this->pon_obxeto(self::__tdata("tdata"));
    $this->pon_obxeto(Panel_fs::__hhmm("thora"));

    $this->pon_obxeto(self::__tdata("tdatapub"));
    $this->pon_obxeto(Panel_fs::__hhmm("thorapub"));

    $this->pon_obxeto(self::__tetq("tetq_1"));
    $this->pon_obxeto(self::__tetq("tetq_2"));
    $this->pon_obxeto(self::__tetq("tetq_3"));

    //* asignamos a $this->pax aos controis.
    $this->obxeto("hcrop" )->post( $this->imx_proporcion );

    $this->obxeto("tnome" )->post( $this->pax->atr("nome"      )->valor           );
    $this->obxeto("tnotas")->post( $this->pax->atr("descricion")->valor           );

// data e hora d noticia/evento  
    $this->obxeto("tdata" )->post( $this->pax->atr("data")->fvalor("Y-m-d") );
    $this->obxeto("thora" )->post( $this->pax->atr("data")->fvalor("H:i"  ) );

// data e hora para programar publicación de noticia
    $this->obxeto("tdatapub" )->post( $this->pax->atr("publicar"  )->fvalor("Y-m-d") );
    $this->obxeto("thorapub" )->post( $this->pax->atr("publicar"  )->fvalor("H:i"  ) );

    $this->obxeto("tetq_1")->post( $this->pax->atr("etq_1"     )->valor           );
    $this->obxeto("tetq_2")->post( $this->pax->atr("etq_2"     )->valor           );
    $this->obxeto("tetq_3")->post( $this->pax->atr("etq_3"     )->valor           );
  }

  private function update(FS_cbd $cbd, $id_idioma) {
//~ print_r($this->pax->a_resumo());

    //* recorta imaxe.
    if (($url = $this->__url_imaxe()) == null) return true;

    //* update.
    if ($this->pax->atr("id_paxina")->valor != null) {
      if (!$this->pax->update($cbd, true, false)) return false;

      return $this->pax->iclogo_update($cbd, $url);
    }

    //* else insert.
    return $this->pax->insert_paxina_detalle($cbd, $id_idioma, $url);
  }
/*
  private function __url_imaxe() {
    if (($url = $this->obxeto("imaxe")->src()) == Imaxe_obd::noimx) return null;
    
    if (($hcrop = $this->obxeto("hcrop")->valor()) == "") return $url; //* non cambiou nada.
    
    list($x, $y, $w, $h) = explode(Escritor_html::csubnome, $hcrop);
    
    
    $fimx = $this->obxeto("f_imaxe");
    
    if ( !$fimx->recortar($x, $y, $w, $h) ) return $url;
    
    
    return $fimx->aceptar_f( $this->url_arquivos );
  }
*/
  private function __url_imaxe() {
    if (($url = $this->obxeto("imaxe")->src()) == Imaxe_obd::noimx) return null;
    
    if (($hcrop = $this->obxeto("hcrop")->valor()) == "") return $url; //* non cambiou nada.
    
    list($x, $y, $w, $h) = explode(Escritor_html::csubnome, $hcrop);
    
    
    $fimx = $this->obxeto("f_imaxe");
    
    if ( !$fimx->recortar($x, $y, $w, $h) ) return $url;
    
    
    return $fimx->f(); //* Imaxe_obd. encargarase da xestión tmp.
  }

  private function validar() {
  }

  private static function __fimaxe($src_tmp) {
    $f = new File("f_imaxe", $src_tmp, 1024 * 1024 * 3);

    $f->clase_css("default", "file_apagado");


    $f->accept      = "image/gif, image/jpg, image/jpeg, image/png";


    return $f;
  }

  private static function __imx(Paxina_novas2_obd $pax) {
    $url = null;
    if ( ($l = $pax->iclogo_obd(new FS_cbd())) != null ) $url = $l->url(false);

    if ($url == null) $url = Imaxe_obd::noimx;


    $i = new Image("imaxe", $url, false);


    return $i;
  }

  private static function __tdata($id) {
    return new DataInput($id);
  }

  private static function __tetq($id) {
    $t = Panel_fs::__text($id, 20, 255);

    return $t;
  }

  private static function __lcambiar() {
    /* $l = Panel_fs::__link("imx_cambiar", "Cambiar la imagen", "texto2_a"); */
    $l = Panel_fs::__link("imx_cambiar", "Cambiar la imagen", "trw-btn");

    $l->title = "Pincha aqu&iacute; para &laquo;cambiar&raquo; la imagen";

    $l->pon_eventos("onclick", "elmt_imx_cambiar(this)");

    return $l;
  }
}

//**********************************************

class Editor_novas2_filtro extends Editor_prgf {
  const ptw = "ptw/paragrafo/editor_novas2_filtro.html";

  private $pax            = null;
  private $url_arquivos   = null;
  private $url_upload     = null;
  private $imx_proporcion = null;


  public function __construct(Prgf_inovas2 $p) {
    parent::__construct($p);

    $this->ptw    = self::ptw;
    $this->titulo = "Filtrado de noticias";

    $this->inicia($p);
  }

  public function operacion_aceptar(Efs_admin $e) {
    $this->prgf->f_txt  = $this->obxeto("txt" )->valor();
    $this->prgf->f_dini = $this->obxeto("dini")->valor();
    $this->prgf->f_dfin = $this->obxeto("dfin")->valor();

//~ echo $this->where();

    $this->prgf->where_aux($this->where());

    $this->prgf->preparar_saida( $e->ajax() );
    
    $e->ajax()->pon_ok(true, "novas2_pechar_editor()");
    

    return $this->operacion_cancelar($e); ; //* non marcamos no editor a opción de actualizar. 
  }

  private function inicia(Prgf_inovas2 $p) {
    $this->pon_obxeto(self::__tdata("dini"));
    $this->pon_obxeto(self::__tdata("dfin"));

    $this->pon_obxeto(Panel_fs::__text("txt", 20, 20));

    $this->obxeto("txt" )->post($p->f_txt );
    $this->obxeto("dini")->post($p->f_dini);
    $this->obxeto("dfin")->post($p->f_dfin);
  }

  private function where() {
    $wh_txt = Valida::buscador_txt2sql($this->obxeto("txt")->valor(), array("a.nome", "a.titulo", "a.descricion", "b.etq_1", "b.etq_2", "b.etq_3"));

    if ($wh_txt == null) $wh_txt = "(1=1)";


    $wh_d0 = Valida::buscador_data2sql("creado", ">=", $this->obxeto("dini")->valor());
    $wh_d1 = Valida::buscador_data2sql("creado", "<=", $this->obxeto("dfin")->valor());

    $where = "{$wh_txt} and {$wh_d0} and {$wh_d1}";


    return str_replace(" and (1=1)", "", $where);
  }

  private static function __tdata($id) {
    return new DataInput($id);
  }
}
