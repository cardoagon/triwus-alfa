<?php

final class INovas2_elmt {
  public static function html(Paxina_novas2_obd $pax, Prgf_inovas2 $prgf, bool $capa_lista = true) {
    //~ $crs    = $prgf->crs_obd;
    $config = $prgf->config;
    $editor = $prgf->pai->__editor;

    $html = LectorPlantilla::plantilla( Refs::url( $config->ptw_elmt("elmt_novas_0") ) );

    $html = str_replace("[btnr]"    , self::html_btnr ($pax, $editor), $html);
    $html = str_replace("[imaxe]"   , self::html_imaxe($pax, $editor), $html);
    $html = str_replace("[ldetalle]", self::html_nome ($pax, $editor), $html);
    $html = str_replace("[data]"    , self::html_data ($pax         ), $html);
    $html = str_replace("[cetq]"    , self::html_etq  ($pax, $editor), $html);
    $html = str_replace("[enotas]"  , self::html_notas($pax         ), $html);

    $html_btnr_eventos = self::html_btnr_eventos($pax, $editor);
    
    $html = "<div {$html_btnr_eventos}>{$html}</div>";
    
    if (!$capa_lista) return $html;

    return "
<div class='celda_elmt_marcado celda_elmt' novas2='{$pax->atr("id_paxina")->valor}'>
  {$html}
</div>";
  }

  private static function html_imaxe(Paxina_novas2_obd $p, $editor) {
    $u = (($l = $p->iclogo_obd( new FS_cbd() )) == null)?"":$l->url();

    $t = $p->atr("titulo")->valor;

    if ($editor) {
      $fclick = self::fclick($p, $editor);

      return "<img style='cursor: pointer;' onclick=\"{$fclick}\" title='{$t}' alt='{$t}' src='{$u}' />";
    }

    $url = self::url($p, $editor);

    return "<a href='{$url}' title='{$t}' alt='{$t}'><img title='{$t}' alt='{$t}' src='{$u}' /></a>";
  }

  private static function html_nome(Paxina_novas2_obd $p, $editor) {
    $t      = $p->atr("nome")->valor;
    $css    = "class='novas_titulo'";

    $fclick = self::fclick($p, $editor);

    return "<h2 {$css} style='cursor: pointer;' onclick='{$fclick}'>{$t}</h2>";
  }

  private static function html_data(Paxina_novas2_obd $p) {
    if (($d = $p->atr("data")->valor) == null) return "";

    if (($l = $p->atr("id_idioma")->valor) == null) return $d;

    $L = Idioma::factory($l);

    return date($L->formato_fecha, strtotime($d));
  }

  private static function html_etq(Paxina_novas2_obd $p, $editor) {
    $html = "";

    for ($i = 0; $i < 3; $i++) {
      $i2 = $i + 1;

      $_etq = null;
      if ($p->atr("etq_{$i2}")->valor != null)
        $_etq = preg_split("/[,;:]+/", trim($p->atr("etq_{$i2}")->valor) );

      if (!is_array($_etq)) continue;

      $html_aux = "";
      foreach ($_etq as $j => $t) {
        if ($t == "") continue;

        $html_aux .= self::etq_link($i, $j, $t, $editor);
      }

      $html .= "<div>{$html_aux}</div>";
    }


    return $html;
  }

  private static function html_notas(Paxina_novas2_obd $p) {
    return strval($p->atr("descricion")->valor);
  }
/*
  private static function html_crs(Prgf_inovas2 $prgf, Paxina_novas2_obd $pax, $editor) {
    if ($editor) {
      $prgf->crs->obxeto("rs_facebook_url")->sup_eventos();
      $prgf->crs->obxeto("rs_twitter_url" )->sup_eventos();
      $prgf->crs->obxeto("rs_linkedin_url")->sup_eventos();
      $prgf->crs->obxeto("rs_whatsapp_url")->sup_eventos();

      return $prgf->crs->html();
    }

    $url = self::url($pax, $editor);

    $prgf->crs->obxeto("rs_facebook_url")->pon_eventos("onclick", "novas2_trw2RS('facebook', '{$url}')");
    $prgf->crs->obxeto("rs_twitter_url" )->pon_eventos("onclick", "novas2_trw2RS('twitter' , '{$url}')");
    $prgf->crs->obxeto("rs_linkedin_url")->pon_eventos("onclick", "novas2_trw2RS('linkedin', '{$url}')");
    $prgf->crs->obxeto("rs_whatsapp_url")->pon_eventos("onclick", "novas2_trw2RS('whatsapp', '{$url}')");


    return $prgf->crs->html();
  }
*/
  private static function html_btnr(Paxina_novas2_obd $p, $editor) {
    if (!$editor) return "";


    $btnr = new Btnr_elmt_novas2($p->atr("id_paxina")->valor);

    return $btnr->html();
  }

  private static function html_btnr_eventos(Paxina_novas2_obd $p, $editor) {
    if (!$editor) return "";

    $idp = $p->atr("id_paxina")->valor;

    return "onmouseover='novas2_elmt_btnr_over({$idp}, true)' onmouseout='novas2_elmt_btnr_over({$idp}, false)'";
  }

  private static function url(Paxina_novas2_obd $p, $editor) {
    if ($editor) return null;

    return $p->action_2(true, true);
  }

  private static function fclick(Paxina_novas2_obd $p, $editor) {
    if ($editor) return "novas2_editar_2_click(" . $p->atr("id_paxina")->valor . ")";

    $url = self::url($p, $editor);

    return "document.location.href = \"{$url}\"";
  }

  private static function etq_link($i, $j, $txt, $editor) {
    $id = "link" . Escritor_html::csubnome . $i . Escritor_html::csubnome . $j;

    //~ $txt = trim($txt);

    $l = new Span($id, $txt);

    if ($i == 0)
      $l->clase_css("default" , "novas_etq_link");
    else
      $l->clase_css("default" , "novas_etq_link_{$i}");


    if (!$editor) $l->pon_eventos("onclick", "prgf_inovas_url_fetq({$i}, '{$txt}')");


    return $l->html();
  }
}
