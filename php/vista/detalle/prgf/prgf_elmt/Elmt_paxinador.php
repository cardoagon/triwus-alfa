<?php

final class Elmt_paxinador extends AElemento {
  private $id_paxina = null;

  public function __construct(Paxinador_obd $pe_obd) {
    parent::__construct($pe_obd);

    $this->pon_obxeto(new Span("smigas", "smigas non iniciado"));
  }

  public function config_usuario(IEfspax_xestor $e = null) {
    parent::config_usuario($e);

    $this->id_paxina = $e->id_paxina();

    $this->config_smigas();
  }

  public function elmt_obd() {
    return $this->obd_base( new Paxinador_obd() );
  }

  protected function pon_btnr() {
    $btnr = new Btnr_elmt();

    $btnr->config_visibles(false, true, true, true);
    
    $this->pon_obxeto($btnr);
  }

  public function editor() {
    return new Edita_elmt_nulo();
  }

  public function operacion(EstadoHTTP $e) {
    if (($e_aux = $this->operacion_lmiga($e)) != null) return $e_aux;

    return parent::operacion($e);
  }

  public function html_propio() {
    $smigas = $this->obxeto("smigas")->html();

    return "<div class='navegador_marco'>{$smigas}</div>";
  }

  private function operacion_lmiga(Efs $e) {
    $a_lmiga = $this->obxetos("lmiga");

    if (count($a_lmiga) == 0) return null;

    foreach ($a_lmiga as $lmiga) {
      if (!$lmiga->control_evento()) continue;

      return Efspax_xestor::operacion_redirect($e, $e->evento()->subnome(0), 1);
    }


    return null;
  }

  private function config_smigas() {
    $this->obxeto("smigas")->post(null);


    $a_migas = Paxinador_obd::a_migas($this->id_paxina);

    if (($ct_html = count($a_migas)) == 0) return;

    $smigas_html = ""; $smigas_json = "";
    foreach ($a_migas as $p=>$_miga) {
      //* 1.- xera migas html
      $lmiga = self::__lmiga($_miga[1], (($_miga[2] == null) || ($ct_html-- == 1)));

      $this->pon_obxeto($lmiga, $_miga[0]);

      $smigas_html .= $lmiga->html();

      //* 2.- xera migas json (https://schema.org/BreadcrumbList)
      if ($smigas_json != null) $smigas_json .= ",";

      $smigas_json .= self::migas_json_1( $p + 1, $_miga[2], $_miga[1] );
    }

    $smigas_json = self::migas_json_0( $smigas_json );

    $this->obxeto("smigas")->post($smigas_html . $smigas_json);
  }

  private static function __lmiga($txt, $readonly = false) {
    $lmiga = new Span("lmiga", $txt);

    $lmiga->readonly = $readonly;

    $lmiga->clase_css("default" , "navegador_lmiga");
    $lmiga->clase_css("readonly", "navegador_lmiga_r");

    $lmiga->envia_submit("onclick");

    return $lmiga;
  }

  private static function migas_json_0($lista_json) {
    return "
<script type=\"application/ld+json\">
{
 \"@context\": \"http://schema.org\",
 \"@type\": \"BreadcrumbList\",
 \"itemListElement\":
 [
  {$lista_json}
 ]
}
</script>";
  }

  private static function migas_json_1($posicion, $url, $nome) {
    return "
      {
       \"@type\": \"ListItem\",
       \"position\": {$posicion},
       \"item\":
       {
        \"@id\": \"{$url}\",
        \"name\": \"{$nome}\"
        }
      }";
  }
}
