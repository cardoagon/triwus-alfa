<?php

final class CCookies extends Componente {
  private static $_ml = array("en" => array(1 => "I agree" , 2 => "Find out more"  , 3 => "Sólo necesarias"),
                              "es" => array(1 => "Aceptar" , 2 => "Más información", 3 => "Sólo necesarias"),
                              "fr" => array(1 => "Accepter", 2 => "Savoir plus"    , 3 => "Sólo necesarias"),
                              "gl" => array(1 => "Aceptar" , 2 => "Saber máis"     , 3 => "Só necesarias"  ),
                              "pt" => array(1 => "Entendí" , 2 => "Saber máis"     , 3 => "Sólo necesarias")
                            );
                            
  public function __construct(Site_config_obd $sc, $id_idioma) {
    parent::__construct("ccookies");

    $this->visible = $sc->atr("cookies")->valor == 1;
    
    if (!$this->visible) return;

    $this->obxeto("illa")->clase_css("default", "ccookies_sites");
    
    if ($id_idioma == null) $id_idioma = "es";

    $this->pon_daviso  ($id_idioma, $sc->atr("id_site")->valor);
    $this->pon_laceptar($id_idioma);
    $this->pon_lVerMais($id_idioma);
    $this->pon_lNecs   ($id_idioma);
  }

  public function operacion(EstadoHTTP $e) {
    if (!$this->visible) return null;

    if ($this->obxeto("bAceptar")->control_evento()) {
      $this->visible = false;

      $this->preparar_saida($e->ajax());

      return $e;
    }

    return null;
  }

  public function preparar_saida(Ajax $a = null) {
    $html = $this->__ptw();

    $this->obxeto("illa")->post($html);

    if ($a == null) return;


    $a->pon($this->obxeto("illa"));

    $this->obxeto("illa")->post(null);
  }

  private function __ptw() {
    if (!$this->visible) return "";

    $daviso   = $this->obxeto("daviso"  )->html();
    $baceptar = $this->obxeto("bAceptar")->html();
    //* $lnecs    = $this->obxeto("lNecs"   )->html();
    $lvermais = $this->obxeto("lVerMais")->html();

    return "<div style='padding: 5px 0;'>{$daviso}&nbsp;&nbsp;&nbsp;{$lvermais}&nbsp;-&nbsp;{$baceptar}.</div>";
    //* return "<div style='padding: 5px 0;'>{$daviso}&nbsp;&nbsp;&nbsp;{$lvermais}&nbsp;&nbsp;{$lnecs}&nbsp;-&nbsp;{$baceptar}.</div>";
  }

  private function pon_daviso($id_idioma, $id_site) {
    $f = "cookies-aviso.html";

    $d = Efs::url_legais($id_site);

    if (!is_dir($d)) mkdir($d);

    if (!is_file("{$d}{$f}")) copy("ptw/paneis/pcontrol/ceditor_legais/{$f}", "{$d}{$f}");

    $d = new Span("daviso", file_get_contents("{$d}{$f}"));

    $this->pon_obxeto($d);
  }

  private function pon_laceptar($id_idioma) {
    $l = new Link("bAceptar", CCookies::$_ml[$id_idioma][1]);

    $l->style      ( "default", "color: #fff; text-decoration: underline;" );
    $l->pon_eventos( "onclick", "ccookies_aceptar_click(this)"             );
    
    
    $this->pon_obxeto($l);
  }

  private function pon_lVerMais($id_idioma) {
    $href = "legal.php#" . md5("legal-6");


    $l = new Link("lVerMais", CCookies::$_ml[$id_idioma][2], "_blank", $href);

    $l->style      ( "default", "color: #fff; text-decoration: underline;" );
    $l->pon_eventos( "onclick", "ccookies_aceptar_click(this)"             );

    $this->pon_obxeto($l);
  }

  private function pon_lNecs($id_idioma) {
    $l = new Link("lNecs", CCookies::$_ml[$id_idioma][3]);

    $l->style      ( "default", "color: #fff; text-decoration: underline;" );
    $l->pon_eventos( "onclick", "ccookies_necesarias_click(this)"          );

    $this->pon_obxeto($l);
  }

}
