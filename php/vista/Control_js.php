<?php

/*************************************************

    Triwus Framework v.0

    Control_js.php

    Author: Carlos Domingo Arias González

    Copyright (C) 2011

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation version 3.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************/

final class Control_js {
//*  private static $instancias = -1;

  private function __construct() {}


/*
  public static function __bfacebook() {
    return "<script>(
              function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];

                if (d.getElementById(id)) return;

                js = d.createElement(s);

                js.id  = id;
                js.src = \"//connect.facebook.net/en_US/all.js#xfbml=1\";

                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk')
            );</script>";
  }

  public static function __btwitter() {
    return "<script>
            !function(d,s,id) {
              var js, fjs = d.getElementsByTagName(s)[0];

              if (!d.getElementById(id)) {
                js = d.createElement(s);

                js.id = id;

                js.src=\"//platform.twitter.com/widgets.js\";

                fjs.parentNode.insertBefore(js,fjs);
              }
            }(document,\"script\",\"twitter-wjs\");
            </script>";
  }
*/
}

