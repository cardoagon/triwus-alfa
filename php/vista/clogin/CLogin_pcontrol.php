<?php

final class CCambiak_pcontrol extends CCambiak {
  const ptw_0 = "ptw/clogin/pcontrol/ccambiakweb_0.html";
  const ptw_1 = "ptw/clogin/pcontrol/ccambiakweb_1.html";
  const ptw_2 = "ptw/clogin/pcontrol/crenovarkweb_0.html";
  const ptw_3 = "ptw/clogin/pcontrol/crenovarkweb_1.html";
  
  public function __construct() {
    parent::__construct();

    $this->visible = false;

    $this->cambiar_idioma( Idioma::factory("es") );
    
    $this->pon_obxeto(CAcceso_pcontrol::__boton("bAceptar" , "Cambiar la contraseña"));
    $this->pon_obxeto(CAcceso_pcontrol::__boton("bAceptar2", "Corfirmar el código"  ));
    
    $this->pon_obxeto(CAcceso_pcontrol::__txt("email"));
    $this->pon_obxeto(CAcceso_pcontrol::__txt("tk"   ));

    $this->pon_obxeto(CAcceso_pcontrol::__link("cambiak"  , "Acceso clientes"));
    $this->pon_obxeto(CAcceso_pcontrol::__link("lreenviar", "Volver a enviar"));

$this->obxeto("lreenviar")->envia_submit("onclick");
  }

  protected function ptw_0() {
    if ($this->renovar) return Refs::url(self::ptw_2);

    return Refs::url(self::ptw_0);
  }

  protected function ptw_2() {
    if ($this->renovar) return Refs::url(self::ptw_3);

    return Refs::url(self::ptw_1);
  }

  public function operacion(EstadoHTTP $e) {
    $this->obxeto("msx_0")->post(null);
    $this->obxeto("msx_1")->post(null);
    $this->obxeto("msx_2")->post(null);

    if ($this->obxeto("bAceptar2")->control_evento()) return $this->operacion_recuperar($e);

    return parent::operacion($e);
  }

  protected function operacion_cambio(Efs $e, $pcontrol = true) {
    $e_aux = parent::operacion_cambio($e);

    if (($erro = $this->obxeto("msx_0")->valor()) != null) {
      $e->post_msx($erro, "e", false);
      
      return $e;
    }

    $this->ptw = $this->ptw_0();

    return new Epcontrol( $e_aux->usuario() );
  }

  protected function operacion_bCancelar(Efs $e) {
    //* 20170529 operacion cancelar xa non exite, non se usa boton.

    if ($this->obxeto("tk")->valor() != null) return $this->operacion_cambio($e);


    $e->post_msx("Debes teclear el código de verificación.", "e", false);


    return $e;
  }

  protected function operacion_cambiak(Efs $e, $renovar = false) {
    $e->cambiar_panel();

    return $e;
  }

  protected function operacion_recuperar(Efs $e) {
    if ($this->ptw == $this->ptw_2()) {
      return $this->operacion_bCancelar($e);
    }

    $this->validar();

    if (($m = $this->obxeto("msx_1")->valor()) != null) {
      $e->post_msx($m, "e", false);
      
      return $e;
    }
    if (($m = $this->obxeto("msx_2")->valor()) != null) {
      $e->post_msx($m, "e", false);
      
      return $e;
    }

    $email = $this->obxeto("email")->valor();
    $k     = $this->obxeto("cknovo")->__k();

    $c = XCCS::inicia_ck_obd($email, $k);
    if (($erro = XCCS::aceptar_ck($email, $k, $c) ) != null) {
      $e->post_msx($erro, "e", false);
    }
    else {
      //~ //* ($this->cambio == null) => email descoñecido. intentamos engañar a un posible hacker

      $this->cambio = null;

      $this->pon_obxeto(new Param("email_activar", $email));

      $this->ptw = $this->ptw_2();
      

      $e->post_msx("El código de verificación ha sido enviado a <b>{$email}</b>.", "m", false);
    }


    return $e;
  }

  protected function operacion_lreenviar(Efs $e) {
    $c = $this->cambio;
    $m = $this->obxeto("email_activar")->valor();

echo "<pre>$m\n" . print_r($c->a_resumo(), 1) .  "</pre>";


    if ($c != null) XCCS::__mail_ck($c, $m)->enviar();


    $e->post_msx("El código de verificación ha sido enviado a <b>{$m}</b>.", "m", false);


    return $e;
  }
}

//****************************

final class CAcceso_pcontrol extends CAcceso {
  const ptw_0 = "ptw/clogin/pcontrol/caccesoweb_0.html";
  const ptw_2 = "ptw/clogin/pcontrol/caccesoweb_1.html";


  public function __construct() {
    parent::__construct();

    $this->cambiar_idioma( Idioma::factory("es") );

    $this->ptw = $this->ptw_0();

    $this->pon_obxeto(self::__link ("cambiak", "Olvidaste tu contraseña"));
    
    $this->pon_obxeto(self::__boton("bAceptar", "Iniciar sesión"));
    
    $this->pon_obxeto(self::__txt("email"));
 //   $this->pon_obxeto(self::__txt("contrasinal", true));
    $this->pon_obxeto(Panel_fs::__password("contrasinal"));

    
    //~ $this->obxeto("email")->envia_submit("onchange");
    
    $this->obxeto("contrasinal")->pon_eventos("onkeypress", "pcontrol_contrasinal_onkeypress(event, this)");
  }

  protected function ptw_0() {
    return Refs::url(self::ptw_0);
  }

  protected function ptw_2() {
    return Refs::url(self::ptw_2);
  }

  protected function operacion_bCancelar(Efs $e) {
    //~ $e->redirect("http://triwus.com");

    return $e;
  }

  protected function operacion_clogin(Efs $e) {
//~ echo $e->evento()->html();

    if ($this->ptw == $this->ptw_2()) return $this->operacion_bCancelar($e);

    $this->validar();

    if (($m = $this->obxeto("msx_1")->valor()) != null) {
      $e->post_msx($m, "e", false);
      
      return $e;
    }
    if (($m = $this->obxeto("msx_2")->valor()) != null) {
      $e->post_msx($m, "e", false);
      
      return $e;
    }

    $u = $e->usuario();

    $cbd = new FS_cbd();

    $r_login = $u->login_pcontrol($cbd, $this->obxeto("email")->valor(), $this->obxeto("contrasinal")->valor());

//~ echo "<pre>" . print_r($r_login, 1) . "</pre>";

    switch ($r_login) {
      case -1:
        $this->ptw = Refs::url(self::ptw_2);

        break;

      case  0:
        //~ echo "<pre>" . print_r($u->a_resumo(), true) . "</pre>";

        if ($u->atr("k_renovar")->valor != null) return $this->operacion_cambiak($e, true);

        //* non e preciso renovar contrasinal.

        Acceso::rexistrar($cbd, 
                          $u->atr("id_site")->valor, 
                          $_SERVER["REMOTE_ADDR"], 
                          $this->obxeto("email")->valor(), 
                          "pcontrol", 
                          1
                          );
        
        return new Epcontrol($u);
        
      default:
        $intentos = 1 + FGS_usuario::log_max_intentos - $r_login;

        $erro = "Error de credenciales.";
        if ($intentos <= 3) $erro .= " Te quedan {$intentos} intentos.";

        $e->post_msx($erro, "e", false);

        break;
    }


    return $e;
  }

  protected function operacion_cambiak(Efs $e, $renovar = false) {
    if ($renovar) $e->post_msx("Es necesario que renueves tu contraseña", "a", false);
    
    $e->obxeto("ccambiak")->inicia($renovar);

    $e->cambiar_panel();


    return $e;
  }

  public static function __link($id, $txt, $title = null) {
    $l = new Link($id, $txt);

    $l->title = $title;

    //~ $l->pon_eventos("onclick", "pcontrol_login_link_onclick(this)");
    //~ $l->envia_SUBMIT("onclick");
    $l->envia_ajax("onclick");


    return $l;
  }

  public static function __boton($id, $txt, $title = null) {
    $b = new Button($id, $txt);

    $b->title = $title;

    return $b;
  }

  public static function __txt($id, $pass = false, $title = null) {
    $t = ($pass)?new Password($id):new Text($id);

    $t->size        = 255;
    $t->title       = $title;
    $t->placeholder = " ";

    $t->clase_css("default", null);

    return $t;
  }

  public static function rlogin_valida($rlogin) {
    $t = ($pass)?new Password($id):new Text($id);

    $t->size        = 255;
    $t->title       = $title;
    $t->placeholder = " ";

    $t->clase_css("default", null);

    return $t;
  }
}

//****************************
/*
final class CAcceso_pcontrol_admin extends CAcceso_pcontrol {
  const ptw_0 = "ptw/clogin/pcontrol/caccesoweb_admin_0.html";
  const ptw_2 = "ptw/clogin/pcontrol/caccesoweb_admin_1.html";


  public function __construct() {
    parent::__construct();

    $this->nome = "acceso_admin";

    $this->pon_obxeto(Panel_fs::__email("email_2", 23, 255, Msx::email_placeholder));
    $this->pon_obxeto(Panel_fs::__password("contrasinal_2", 23, 255, Msx::passwd_placeholder_0));

    $this->obxeto("email_2")->style("default", "width: 99%;");
    $this->obxeto("contrasinal_2")->style("default", "width: 99%;");
  }

  protected function ptw_0() {
    return Refs::url(self::ptw_0);
  }

  protected function ptw_2() {
    return Refs::url(self::ptw_2);
  }

  public function operacion_clogin(Efs $e) {
    if ($this->ptw == self::ptw_2) return $e;


    $this->validar();

    if ($this->obxeto("msx_1")->valor() != null) {
      return $e;
    }
    if ($this->obxeto("msx_2")->valor() != null) {
      return $e;
    }


    $cbd = new FS_cbd();

    $u   = $e->usuario();

    $e1  = $this->obxeto("email"        )->valor();
    $e2  = $this->obxeto("email_2"      )->valor();
    $k1  = $this->obxeto("contrasinal"  )->valor();
    $k2  = $this->obxeto("contrasinal_2")->valor();


    $r_login = $u->login_pcontrol_a($cbd, $e1, $e2, $k1, $k2);

    switch ($r_login) {
      case -1:
        $this->ptw = Refs::url(self::ptw_2);

        $this->pon_obxeto(Panel_fs::__baceptar());

        break;

      case  0:
        $this->obxeto("email_2"      )->post("");
        $this->obxeto("contrasinal"  )->post("");
        $this->obxeto("contrasinal_2")->post("");

        return new Epcontrol($u);

      default:
        $erro = Erro::login;

        $intentos = 1 + FGS_usuario::log_max_intentos - $u->login_intentos();

        if ($intentos <= 3) $erro .= "<br />Te quedan {$intentos} intentos.";

        $this->obxeto("msx_0")->post($erro);

        break;
    }


    return $e;
  }

  protected function operacion_cambiak(Efs $e, $renovar = false) {
    $e->cambiar_panel();

    return $e;
  }

  protected function validar_email() {
    $email_1 = $this->obxeto("email")->valor();
    $email_2 = $this->obxeto("email_2")->valor();

    $erro_1 = "";
    $erro_2 = "";

    if (!Valida::email($email_1)) $erro_1 .= "<div>" . Erro::email . "</div>";
    if (!Valida::email($email_2)) $erro_2 .= "<div>" . Erro::email . "</div>";

    $this->obxeto("msx_1")->post($erro_1 . $erro_2);
  }

  protected function validar_k() {
    $k_1 = $this->obxeto("contrasinal")->valor();
    $k_2 = $this->obxeto("contrasinal_2")->valor();

    $erro_1 = "";
    $erro_2 = "";

    if ($k_1 == "") $erro_1 .= "<div>" . Erro::passwd_baleiro . "</div>";
    if ($k_2 == "") $erro_2 .= "<div>" . Erro::passwd_baleiro . "</div>";

    $this->obxeto("msx_2")->post($erro_1 . $erro_2);
  }
}

*/
