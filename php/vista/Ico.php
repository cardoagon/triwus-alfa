<?php

/*************************************************

    Triwus Framework v.0

    Ico.php

    Author: Carlos Domingo Arias González

    Copyright (C) 2011

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation version 3.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************/

final class Ico {
  const badd              = "imx/accions/bAdd.png";
  const baddpax           = "imx/accions/bAddpax.png";
  const balign_hl         = "imx/accions/bAlign_hl.png";
  const balign_hc         = "imx/accions/bAlign_hc.png";
  const balign_hr         = "imx/accions/bAlign_hr.png";
  const balign_vt         = "imx/accions/bAlign_vt.png";
  const balign_vc         = "imx/accions/bAlign_vc.png";
  const balign_vb         = "imx/accions/bAlign_vb.png";
  const bapply            = "imx/accions/bApply.png";
  const battention        = "imx/accions/bAttention.png";
  const bautoresposta     = "imx/accions/mail-autoresposta.svg";
  const bautoresposta2    = "imx/accions/mail-autoresposta2.svg";
  const bbuscar           = "imx/accions/bBuscar.png";
  const bcancel           = "imx/accions/bCancel.png";
  const bcandado          = "imx/accions/candado.svg";
  const bcandado2         = "imx/accions/candado2.svg";
  const bcarro            = "imx/accions/bcarro_elmt.png";
  const bcarrusel         = "imx/accions/bCarrusel.png";
  const bchave            = "imx/accions/chave.svg";
  const bcontacto         = "imx/accions/bcontacto.png";
  const bcomentario       = "imx/accions/bComentario.png";
  const bdescargar        = "imx/accions/bdescargar.svg";
  const bdifucom          = "imx/accions/bDifucom.png";
  const bedit             = "imx/accions/bEdit.png";
  const bedit2            = "imx/accions/bEdit2.png";
  const bedit3            = "imx/accions/edit-copy-2.svg";
  const beditcopy         = "imx/accions/bEditCopy.png";
  const benquisa          = "imx/accions/bEnquisa.png";
  const bfacebook         = "imx/accions/bFacebook.png";
  const bgmais            = "imx/accions/bGoogle.png";
  const bhr               = "imx/accions/bHR.png";
  const bico              = "imx/iconoweb.png";
  const bindice           = "imx/accions/bIndice.png";
  const bimaxe            = "imx/accions/bImaxe.png";
  const bgaleria          = "imx/accions/bImaxe.png";
  const blink             = "imx/accions/bLink.png";
  const blink2            = "imx/accions/enlace.svg";
  const blinkedin         = "imx/accions/bLinkedin.png";
  const bmail             = "imx/accions/bMail.png";
  const bmailsend         = "imx/accions/bMail_send.png";
  const bmais             = "imx/accions/bMais.png";
  const bmenu             = "imx/accions/bMenu.png";
  const bmobil            = "imx/accions/bMobil.jpg";
  const bmobil2           = "imx/accions/bMobil2.jpg";
  const bmove             = "imx/accions/bMove.png";
  const bmove_subir       = "imx/accions/bMove_subir.png";
  const bmove_subirnivel  = "imx/accions/bMove_subirnivel.png";
  const bmove_baixar      = "imx/accions/bMove_baixar.png";
  const bmove_baixarnivel = "imx/accions/bMove_baixarnivel.png";
  const bnovo             = "imx/accions/novo.png";
  const bopinion          = "imx/accions/bopinion.png";
  const bpaso_ant         = "imx/accions/bpaso_anterior.png";
  const bpaso_fin         = "imx/accions/bpaso_fin.png";
  const bpaso_ini         = "imx/accions/bpaso_ini.png";
  const bpaso_seg         = "imx/accions/bpaso_seguinte.png";
  const bprev             = "imx/accions/bPrev.png";
  const bpreview          = "imx/accions/bPreview.png";
  const bpreview2         = "imx/accions/bPreview2.png";
  const brama_mais        = "imx/accions/bRama_mais.png";
  const brama_mais2       = "imx/accions/bRama_mais2.png";
  const brama_menos       = "imx/accions/bRama_menos.png";
  const brama_menos2      = "imx/accions/bRama_menos2.png";
  const brama_fin         = "imx/accions/bRama_fin.png";
  const brecortar         = "imx/accions/bRecortar.png";
  const brec              = "imx/accions/bRec.png";
  const breload           = "imx/accions/reload.svg";
  const brevert           = "imx/accions/bRevert.png";
  const bsave             = "imx/accions/bSave.png";
  const bsave2            = "imx/accions/bSave2.png";
  const bsave3            = "imx/accions/bSave3.png";
  const bsup              = "imx/accions/bEliminar.png";
  const btrash            = "imx/accions/btrash.png";
  const btrash2           = "imx/accions/btrash2.png";
  const btrash3           = "imx/accions/borrar.svg";
  const btxt              = "imx/accions/bText.png";
  const btwitter          = "imx/accions/bTwitter.png";
  const bverweb           = "imx/accions/bVerWeb.png";

  const xpax_libre        = "imx/pcontrol/xpax/libre.png";
  const xpax_novas        = "imx/pcontrol/xpax/novas.png";
  const xpax_album        = "imx/pcontrol/xpax/album.png";
  const xpax_ventas       = "imx/pcontrol/xpax/ventas.png";
  const xpax_ref          = "imx/pcontrol/xpax/ref.png";
  const xpax_reservada    = "imx/pcontrol/xpax/reservada.png";

  const mime_audio        = "imx/mime/audio.png";
  const mime_excel        = "imx/mime/x-office-spreadsheet.png";
  const mime_folder       = "imx/mime/folder.png";
  const mime_image        = "imx/mime/image.png";
  const mime_package      = "imx/mime/package.png";
  const mime_pdf          = "imx/mime/pdf.png";
  const mime_text         = "imx/mime/text.png";
  const mime_video        = "imx/mime/video.png";

  const espera            = "imx/espera.gif";
  const nologo            = "imx/nologo.png";
  const transpa           = "imx/transpa.png";
}
