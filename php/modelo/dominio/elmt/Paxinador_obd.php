<?php


class Paxinador_obd extends Elmt_obd {

  const tipo_bd = "PAXINADOR";

  public function __construct() {
    parent::__construct();
  }

  public static function crea_migaspan() {
    return new Paxinador_obd();
  }

  public function mapa_bd() {
    return new Elmt_mbd();
  }

  protected function tipo_bd() {
    return self::tipo_bd;
  }
  
  public static function a_migas($id_paxina, $url_limpa = false, FS_cbd $cbd = null) {
    if ($cbd == null) $cbd = new FS_cbd();

    $p = Paxina_obd::inicia($cbd, $id_paxina);

    if ($p->atr("id_paxina")->valor == null) return null;

    $_a0 = array();

    do {
      $action = ($p->atr("estado")->valor == "publicada")?$p->action_2($url_limpa, true):null;
      
      $_a1 = array($p->atr("id_paxina")->valor, $p->atr("nome")->valor, $action);

      array_unshift($_a0, $_a1);

      $p = $p->select_pai($cbd);
    }
    while ($p != null);


    return $_a0;
  }
}
