<?php

class Cuenta_obd extends Obxeto_bd  {
  public function __construct() {
    parent::__construct();
  }

  public function mapa_bd() {
    return new Cuenta_mbd();
  }

  public function edita_web() {
    switch ($this->atr("id_sb")->valor) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
        return true;
    }

    return false;
  }

  public static function inicia(FS_cbd $cbd, $id_servizo) {
    $u = new Cuenta_obd();

    $u->select($cbd, "v_servizo_plan.id_servizo = {$id_servizo}");

    return $u;
  }

  public static function inicia_usuario(FS_cbd $cbd, $id_usuario) {
    $u = new Cuenta_obd();

    $u->select($cbd, "v_servizo_plan.id_usuario = {$id_usuario}");
    
    if ($u->atr("dominio")->valor != null) return $u;
    
    
    //* debemos enlacer con site_plesk tomando como clave id_usuario.
    
    $r = $cbd->consulta("select dominio from site_plesk where id_usuario = {$id_usuario}");
    
    
    if (!$_a = $r->next()) return $u;
    
    
    $u->atr("dominio")->valor = $_a["dominio"];


    return $u;
  }

  public function select(FS_cbd $cbd, $where = null, $orderby = null) {
    $r = $cbd->consulta($this->sql_select($cbd, $where, $orderby));

    if (!$a = $r->next()) return;

    $this->post($a);
  }

  public function _maxGB(FS_cbd $cbd = null) {
    if ($cbd == null) $cbd = new FS_cbd();

    $sql = "select a.* 
              from site_plus a inner join v_control_dias b on (a.id_site = b.id_site)
             where (b.dias > 0) and (a.id_sb = b.id_sb) and (a.id_site = " . $this->atr("id_site")->valor . ")";

    $r = $cbd->consulta($sql);

    if ($_a = $r->next()) return $_a;

    return null;
  }

  public function _subs(FS_cbd $cbd = null):array {
    if (($u = $this->atr("id_usuario")->valor) == null) return [];
    
    if ($cbd == null) $cbd = new FS_cbd();

    $sql = "select a.nome, a.version 
              from servizo a 
             where (a.id_usuario = {$u}) and (id_sb = '121000')";

    $r = $cbd->consulta($sql);

    $_subs = [];
    while ($_r = $r->next()) $_subs[] = $_r;

    return $_subs;
  }

  //~ public function _subs(FS_cbd $cbd = null) {
    //~ if ($cbd == null) $cbd = new FS_cbd();

    //~ $sql = "select a.* from site_plus a where (a.id_site = " . $this->atr("id_site")->valor . ")";

    //~ $r = $cbd->consulta($sql);

    //~ if ($_a = $r->next()) return $_a;

    //~ return null;
  //~ }
}

//-----------------------------------------------

final class Cuenta_mbd extends Mapa_bd {
  public function __construct() {
    $t = new Taboa_dbd("v_servizo_plan");

    $t->pon_campo("id_site"      , new Numero());
    $t->pon_campo("id_servizo"   , new Numero());
    $t->pon_campo("id_usuario"   , new Numero());
    $t->pon_campo("id_sb"        , new Numero());
    $t->pon_campo("nome");
    $t->pon_campo("max_GB"       , new Numero());
    $t->pon_campo("multilinguaxe", new Numero());
    $t->pon_campo("venta"        , new Numero());
    $t->pon_campo("email"        , new Numero());
    $t->pon_campo("pvp_base"     , new Numero());
    $t->pon_campo("pvp_frac"     , new Numero());
    $t->pon_campo("caduca"       , new Numero());
    $t->pon_campo("custodia"     , new Numero()); //* tiempo de custodia m&iacute;nimo segun LOPD

    $this->pon_taboa($t);


    $t2 = new Taboa_dbd("site_plesk");

    $t2->pon_campo("dominio");

    $this->relacion_fk($t2, array("id_site"), array("id_site"), "left");
  }
}

