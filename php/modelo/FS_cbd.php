<?php

final class FS_cbd extends MySql {
  public function __construct($verbose = false) {
    parent::__construct(Refs::mysql_h, Refs::mysql_u, Refs::mysql_p, Refs::mysql_b);

    $this->verbose = $verbose;
  }
}

