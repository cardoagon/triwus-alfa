<?php

final class Refs {
  
  //* proposta.
  const url_home                  = "triwus.com";
  const url_protexida             = "triwus";
  const url_symlink               = "/var/www/vhosts/triwus.com/httpdocs";
  const url_arquivo_priv          = "../triwus-priv";
  const url_tmp                   = "tmp";
  const url_tmp2                  = "/var/www/vhosts/triwus.com/tmp/";

  const mysql_h                   = "localhost";
  const mysql_b                   = "mytriwus";
  const mysql_u                   = "mytriwus";
  const mysql_p                   = "#QqFY22W3";


  //* non configurables.
  const url_sites                 = "sites";
  const url_arquivo               = "arquivo";
  const url_panoramix             = "arquivo/panoramix";
  const url_legais                = "legais";
  const url_arquivos              = "arquivos";
  const url_galerias              = "arquivos/galerias";

  const url_logs                  = "/var/www/vhosts/triwus.com/logs";

  public static function fs_home() {
    return getenv("fs_home");
  }

  public static function protocolo($https = -1) {
    if ($https == 1) return "https://";
    if ($https == 0) return "http://";

    if (strtoupper(getenv("server.https")) != "ON") return "http://";

    return "https://";
  }

  public static function url_home($https = -1) {
    return self::protocolo($https) . self::url_home;
  }

  public static function snome($https = -1) {
    $snome = getenv("server.nome");

    if (strtolower($snome) == "localhost") return self::url_home($https);
    if (self::valida_IPv4($snome))         return self::url_home($https);


    return self::protocolo($https) . $snome;
  }

  public static function url($url) {
    return self::fs_home() . $url;
  }

  public static function url_2($url, $refsite) {
    $url = Refs::url_arquivos . "/{$url}";
    
    if (self::fs_home() != "") return $url;
    
    return Refs::url_sites . "/{$refsite}/{$url}";
  }

  public static function url_split($url) {
    if ($url == null) return null;


    list($x, $src_0)     = explode("/" . self::url_arquivos . "/", $url);
    list($src, $extmini) = explode(".", $src_0);

    $ref_site = str_replace(self::url_sites . "/", "", str_replace("triwus/", "", $x));

    return array(0=>$ref_site, 1=>$src, "ref_site"=>$ref_site, "url"=>$src);
  }

  private static function valida_IPv4($ip) {
    $long = ip2long($ip);

    return !($long == -1 || $long === FALSE);
  }
}
