<?php

final class Refs {
  //* proposta.
  const url_home            = "127.0.0.1/triwus";
  //~ const url_home            = "192.168.43.250/triwus";
  const url_protexida       = "triwus";
  const url_symlink         = "/var/www/html/triwus";
  const url_arquivo_priv    = "triwus-priv";
  const url_tmp             = "tmp";
  const url_tmp2            = "/var/www/html/triwus/tmp/";

  const mysql_h             = "127.0.0.1";
  const mysql_b             = "mytriwus2";
  const mysql_u             = "mytriwus";
  const mysql_p             = "1234qwer";


  //* non configurables.
  const url_sites           = "sites";
  const url_stmp            = "tmp";
  const url_arquivo         = "arquivo";
  const url_legais          = "legais";
  const url_arquivos        = "arquivos";
  //~ const url_galerias        = "arquivo/galerias";
  const url_galerias        = "arquivos/galerias";
  const url_panoramix       = "arquivo/panoramix";

  const url_logs            = "/var/log/apache2/access*";


  public static function fs_home() {
    return getenv("fs_home");
  }

  public static function protocolo($https = -1) {
    if ($https == 1) return "https://";
    if ($https == 0) return "http://";

    if (strtoupper(getenv("server.https")) != "ON") return "http://";

    return "https://";
  }

  public static function url_home($https = -1) {
    return self::protocolo($https) . self::url_home;
  }

  public static function snome($https = -1) {
    $snome = getenv("server.nome");

    if (strtolower($snome) == "localhost") return self::url_home($https);
    if (self::valida_IPv4($snome))         return self::url_home($https);


    return self::protocolo($https) . $snome;
  }

  public static function url($url) {
    return self::fs_home() . $url;
  }

  public static function url_2($url, $refsite) {
    $url = Refs::url_arquivos . "/{$url}";

    if (self::fs_home() != "") return $url;

    return Refs::url_sites . "/{$refsite}/{$url}";
  }

  public static function url_split($url) {
    if ($url == null) return null;


    list($x  , $src_0  ) = explode("/" . self::url_arquivos . "/", $url);
    list($src, $extmini) = explode(".", $src_0);

    $ref_site = str_replace(self::url_sites . "/", "", str_replace("triwus/", "", $x));

    return array(0=>$ref_site, 1=>$src, "ref_site"=>$ref_site, "url"=>$src);
  }

  private static function valida_IPv4($ip) {
    $long = ip2long($ip);

    return !($long == -1 || $long === FALSE);
  }
}
