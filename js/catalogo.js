function trwCat_0000(){const panel=document.getElementById("trwCat_cbusca_3");const backdrop=document.getElementById("trwCat_backdrop");if(!panel)return;if(!backdrop)return;panel.classList.toggle("open")?(backdrop.style.display="block"):(backdrop.style.display="none")}
function trwCat_colorea(){const urlParams=new URLSearchParams(window.location.search);const iconoFiltros=document.getElementById("trwCat_cbusca_2");const filtros=["f0","f1","f2","f3"];filtros.forEach((filtro)=>{if(urlParams.has(filtro)&&urlParams.get(filtro)!==""){iconoFiltros.classList.add("cxcli3_filter");return}})}
function trwCat_reset(action){var editor=document.getElementById("findex;editor").value==1;if(editor)return;document.location.href=action}
function trwCat_filtrar(action){var editor=document.getElementById("findex;editor").value==1;if(editor)return;document.location.href=trwCat_f(action)}
function trwCat_paxinar(o,action,pax){var editor=document.getElementById("findex;editor").value==1;if(editor){enviaEventoAJAX(o,"onclick");return}
document.location.href=trwCat_f(action)+`&pax=${pax}`}
function trwCat_paxinar_txt(o,action,t){var pax=parseInt(o.value);if(isNaN(pax))return;pax=Math.abs(pax);if(pax>t)return;return trwCat_paxinar(o,action,pax)}
function trwCat_f(action){var f=action;var f_aux=null;if((f_aux=trwCat_f_a())!="")f+="&f0="+f_aux;if((f_aux=trwCat_f_b("ctlg_busca_etq_1"))!="")f+="&f1="+f_aux;if((f_aux=trwCat_f_b("ctlg_busca_etq_2"))!="")f+="&f2="+f_aux;if((f_aux=trwCat_f_b("ctlg_busca_etq_3"))!="")f+="&f3="+f_aux;return f}
function trwCat_f_a(){var t=document.querySelector("input[ctlg_busca_etq_0]");if(t==null)return"";return encodeURIComponent(t.value)}
function trwCat_f_b(id){var d=document.getElementById(id);if(d==null)return"";var _h=d.getElementsByTagName("input");var f_aux="";var _v=null;for(var i=0;i<_h.length;i++){if(_h[i].value!=1)continue;if(f_aux!="")f_aux+=",";_v=_h[i].id.split(csubnome);f_aux+=_v[_v.length-1]}
return f_aux}