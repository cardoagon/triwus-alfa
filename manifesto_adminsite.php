<?php


require "php/control/Xhtaccess.php";


require "php/control/admin/Log_eventos.php";
require "php/control/admin/Efs_admin.php";


require "php/control/admin/consulta/Econsulta.php";
require "php/control/admin/consulta/Econsulta_site.php";
//~ require "php/control/admin/consulta/Econsulta_k.php";

require "php/control/admin/Epcontrol.php";
require "php/control/admin/Epaxina_edit.php";


require "php/rpc/piwik/Piwik_trw.php";

require "php/rpc/plesk/Plesk.php";
require "php/rpc/plesk/PleskMail.php";
require "php/rpc/plesk/Plesk_siteinfo.php";


require "php/vista/admin/clopd_cancela/Clopd_cancela.php";

require "php/sitemap/XSiteMap.php";

require "php/excelwriter/ExcelWriterXML.php";
require "php/excelwriter/ExcelWriterXML_Sheet.php";
require "php/excelwriter/ExcelWriterXML_Style.php";

require "php/vista/vm/VM-pcontrol-aux.php";

require "php/vista/vm/VM-editor-confirmar.php";
require "php/vista/vm/VM-editor-esperar.php";
require "php/vista/vm/VM-editor-recargar.php";

require "php/vista/admin/paneis/Panel_site_cab.php";
require "php/vista/admin/paneis/Panel_site_pe.php";

require "php/vista/admin/paneis/pcontrol/PControl.php";
require "php/vista/admin/paneis/pcontrol/CURexistrados.php";
require "php/vista/admin/paneis/pcontrol/xeral/CMetatags.php";
require "php/vista/admin/paneis/pcontrol/estilo/Selector_config.php";
require "php/vista/admin/paneis/pcontrol/CMultilinguaxe.php";
require "php/vista/admin/paneis/pcontrol/ceditlegais/CEditor_legais.php";
require "php/vista/admin/paneis/pcontrol/CRedesSoc.php";
require "php/vista/admin/paneis/pcontrol/CCliEmail.php";
require "php/vista/admin/paneis/pcontrol/CMail.php";
require "php/vista/admin/paneis/pcontrol/Cstats.php";
require "php/vista/admin/paneis/pcontrol/accesos/Clogs_apache.php";
require "php/vista/admin/paneis/pcontrol/accesos/Clogs_triwus.php";
require "php/vista/admin/paneis/pcontrol/CApis.php";

require "php/vista/admin/paneis/pcontrol/apis/CApiTriwus.php";
require "php/vista/admin/paneis/pcontrol/apis/CApiDbox.php";
require "php/vista/admin/paneis/pcontrol/apis/CFscripts.php";
require "php/vista/admin/paneis/pcontrol/apis/CApk.php";

require "php/vista/admin/paneis/pcontrol/usuario/CServizos.php";
require "php/vista/admin/paneis/pcontrol/usuario/Cusuario.php";
require "php/vista/admin/paneis/pcontrol/arquivo/Carquivo.php";
require "php/vista/admin/paneis/pcontrol/arquivo/CDropbox.php";
require "php/vista/admin/paneis/pcontrol/ventas/CConfig_ventas.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxarticulos.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxarticulos_ficha.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxarticulos_ficha_cc.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxarticulos_ficha_imaxes.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxarticulos_ficha_presuposto.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxarticulos_importar.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxarticulos_masivo.php";
//~ require "php/vista/admin/paneis/pcontrol/ventas/Cxarticulos_relacions.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxfp_config.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxforma_pago.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxclientes.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxpedidos.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxcamposcustom.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxalmacen.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxalmacen_portes.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxmarca.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxcategoria.php";
//~ require "php/vista/admin/paneis/pcontrol/ventas/Cxpromos_arti.php";
require "php/vista/admin/paneis/pcontrol/ventas/Cxpromos.php";

require "php/vista/admin/paneis/paxina/Panel_paxina_edicion.php";

